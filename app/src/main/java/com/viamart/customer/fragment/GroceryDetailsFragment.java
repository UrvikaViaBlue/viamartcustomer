package com.viamart.customer.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.viamart.customer.R;
import com.viamart.customer.activity.AddressBookActivity;
import com.viamart.customer.activity.DrawerProfileActivity;
import com.viamart.customer.activity.ProfileActivity;
import com.viamart.customer.adapter.AddToCartDataAdapter;
import com.viamart.customer.adapter.AddedItemListForCartAdapter;
import com.viamart.customer.adapter.GroceryDetailsAdapter;
import com.viamart.customer.adapter.VariantItemAdapter;
import com.viamart.customer.db.AddToCartDataHelper;
import com.viamart.customer.interfaceData.IOnBackPressed;
import com.viamart.customer.model.AddtoCartData;
import com.viamart.customer.model.PlaceOrderModel;
import com.viamart.customer.model.PlaceOrderResponse;
import com.viamart.customer.model.StoreCategory;
import com.viamart.customer.model.StoreProductDetails;
import com.viamart.customer.model.TempPriceStore;
import com.viamart.customer.retrofit.ApiInstant;
import com.viamart.customer.retrofit.BaseRetrofit;
import com.viamart.customer.utils.LocationTrack;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.LOCATION_SERVICE;


public class GroceryDetailsFragment extends Fragment implements View.OnClickListener, IOnBackPressed {

    @BindView(R.id.groceryDetailsRv)
    RecyclerView groceryDetailsRv;
    private ActionBar toolbar;
    @BindView(R.id.llBottom)
    LinearLayout llBottom;
    @BindView(R.id.cart_show_bottom_sheet_cust)
    ConstraintLayout cart_show_bottom_sheet_cust;
    @BindView(R.id.added_item_Data)
    LinearLayout added_item_Data;
    @BindView(R.id.txtPriceVariant)
    AppCompatTextView txtPriceVariant;
    @BindView(R.id.relativeAdd)
    RelativeLayout relativeAdd;
    @BindView(R.id.viwBottom)
    View viwBottom;
    @BindView(R.id.btnViewCart)
    AppCompatTextView btnViewCart;
    @BindView(R.id.img_close)
    AppCompatImageView img_close;
    @BindView(R.id.btn_locationChange)
    AppCompatTextView btn_locationChange;
    private BottomSheetBehavior mBottomSheetBehavior1;
    @BindView(R.id.txt_productName)
    AppCompatTextView txt_productName;
    @BindView(R.id.txt_itemQuantity)
    AppCompatTextView txt_itemQuantity;
    @BindView(R.id.Txt_MinusQuantity)
    AppCompatTextView Txt_MinusQuantity;
    @BindView(R.id.txt_AddQuantity)
    AppCompatTextView txt_AddQuantity;
    @BindView(R.id.rg_variant)
    RecyclerView rg_variant;
    private boolean myCondition;
    private List<AddtoCartData> addtoCartDataList = new ArrayList<>();
    private List<PlaceOrderModel> placeOrderModels = new ArrayList<>();
    private RecyclerView rvOrderData;
    private AppCompatTextView txtTotalPrice;
    private AppCompatButton btnPlaceOrder;
    Location mLocation;
    private AppCompatTextView txt_currentAddress;
    @BindView(R.id.txt_itemCount)
    AppCompatTextView txt_itemCount;
    @BindView(R.id.txt_totalPrice)
    AppCompatTextView txt_totalPrice;


    @BindView(R.id.txtProductNameAdded)
    AppCompatTextView txtProductNameAdded;
    @BindView(R.id.img_close_addedData)
    AppCompatImageView img_close_addedData;
    @BindView(R.id.addedItemData)
    RecyclerView addedItemData;
    @BindView(R.id.txtAddNewItemCustomization)
    AppCompatTextView txtAddNewItemCustomization;

    public static Fragment newInstance() {
        return null;
    }

    private List<StoreCategory.Product> storeProductList;
    private int Store_ID;
    private double totalAmount;
    private String phoneNumber, android_device_id, category;
    private List<TempPriceStore> tempPriceStoreList = new ArrayList<>();
    private String price = null;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @SuppressLint({"WrongConstant", "ClickableViewAccessibility"})
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_grocery_details, container, false);
        ButterKnife.bind(this, view);

        SharedPreferences sh = getContext().getSharedPreferences("MySharedPref", Context.MODE_APPEND);
        phoneNumber = sh.getString("phoneNumber", "");
        android_device_id = sh.getString("Device_ID", "");
        Store_ID = sh.getInt("Store_ID", 0);
        storeProductList = (List<StoreCategory.Product>) getArguments().getSerializable("List");
        category = (String) getArguments().getSerializable("category_id");
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cart_show_bottom_sheet_cust.setVisibility(View.GONE);
            }
        });
        GroceryDetailsAdapter groceryDetailsAdapter = new GroceryDetailsAdapter(storeProductList, addtoCartDataList, getContext(), new GroceryDetailsAdapter.OnItemClickListener() {
            @Override
            public void onItemClickAddItem(View view, int position, int quantity) {
                Log.d("TAG", "onItemClickAddItem: " + quantity);
                AddtoCartData addtoCartData = new AddtoCartData();
                if (quantity >= 2) {

                    if (addtoCartDataList.size() > 0) {
                        cart_show_bottom_sheet_cust.setVisibility(View.GONE);
                        added_item_Data.setVisibility(View.VISIBLE);

                        txtProductNameAdded.setText(addtoCartDataList.get(position).getStoreProductData().getProductName());
                        img_close_addedData.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                added_item_Data.setVisibility(View.GONE);
                            }
                        });
                        //
                        AddedItemListForCartAdapter addedItemListForCartAdapter = new AddedItemListForCartAdapter(getContext(), addtoCartDataList, new AddedItemListForCartAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClickAddItem(View view, int position, int quantity) {

                            }
                        });
                        RecyclerView.LayoutManager mLayoutManagershopName = new LinearLayoutManager(getContext());
                        addedItemData.setLayoutManager(mLayoutManagershopName);
                        addedItemData.setItemAnimator(new DefaultItemAnimator());
                        addedItemData.setAdapter(addedItemListForCartAdapter);


                        txtAddNewItemCustomization.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                added_item_Data.setVisibility(View.GONE);
                                cart_show_bottom_sheet_cust.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                } else {
                    cart_show_bottom_sheet_cust.setVisibility(View.VISIBLE);
                    txt_productName.setText(storeProductList.get(position).getProductName());


                    if (storeProductList.get(position).getVariantName().size() > 0) {
                        LinearLayoutManager recyclerLayoutManager = new LinearLayoutManager(getContext());
                        rg_variant.setLayoutManager(recyclerLayoutManager);
                        VariantItemAdapter variantItemAdapter = new VariantItemAdapter(getContext(), storeProductList.get(position).getVariantName(), new VariantItemAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(String price, int position) {
                                for (int i = 0; i < storeProductList.get(position).getVariantName().size(); i++) {
                                    if (i == position) {
                                       String  varinatName = storeProductList.get(position).getVariantName().get(i).getVariantName();
                                       double priceVariant = storeProductList.get(position).getVariantName().get(i).getProductPrice();
                                        addtoCartData.setVariantName(varinatName);
                                        addtoCartData.setVariantPrice(priceVariant);
                                    }
                                }
                                if (!price.isEmpty()) {
                                    txtPriceVariant.setText(price);
                                    relativeAdd.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            TempPriceStore tempPriceStore = new TempPriceStore();
                                            tempPriceStore.setPrice(Double.parseDouble(price));
                                            tempPriceStoreList.add(tempPriceStore);
                                            double total = 0;
                                            for (int i = 0; i < tempPriceStoreList.size(); i++) {
                                                total += tempPriceStoreList.get(i).getPrice();
                                            }
                                            cart_show_bottom_sheet_cust.setVisibility(View.GONE);
                                            llBottom.setVisibility(View.VISIBLE);

                                            txt_totalPrice.setText(String.valueOf(total));
                                        }
                                    });
                                }
                            }
                        });
                        rg_variant.setAdapter(variantItemAdapter);
                    }



                    addtoCartData.setQuantity(quantity);
                    addtoCartData.setStoreProductData(storeProductList.get(position));

                    addtoCartDataList.add(addtoCartData);

                    PlaceOrderModel placeOrderModel = new PlaceOrderModel();
                    placeOrderModel.setQuantity(quantity);
                    placeOrderModel.setProductId(storeProductList.get(position).getProductId());
                    placeOrderModel.setSpId(storeProductList.get(position).getSpId());
                    placeOrderModel.setVariantPrice(storeProductList.get(position).getProductPrice());
                    placeOrderModels.add(placeOrderModel);

                    AddToCartDataHelper.getInstance(getActivity()).onAddPlaceOrder(addtoCartData);


                    String size = String.valueOf(addtoCartDataList.size());
                    txt_itemCount.setText(size);
                }
            }

        });
        LocationManager mLocationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);

        RecyclerView.LayoutManager mLayoutManagershopName = new LinearLayoutManager(getContext());
        groceryDetailsRv.setLayoutManager(mLayoutManagershopName);
        groceryDetailsRv.setItemAnimator(new DefaultItemAnimator());
        groceryDetailsRv.setAdapter(groceryDetailsAdapter);
        if (ContextCompat.checkSelfPermission(

                getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(

                        getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1,
                    1, mLocationListener);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    1);
        }

        btnViewCart.setOnClickListener(this);
        btn_locationChange.setOnClickListener(this);
        View bottomSheet = view.findViewById(R.id.cart_show_bottom_sheet);
        rvOrderData = view.findViewById(R.id.rvOrderData);

        txtTotalPrice = view.findViewById(R.id.txtTotalPrice);
        btnPlaceOrder = view.findViewById(R.id.btnPlaceOrder);
        btnPlaceOrder.setOnClickListener(this);
        //    AppCompatTextView txtDiscountPrice = view.findViewById(R.id.txtDiscountPrice);
        txt_currentAddress = view.findViewById(R.id.txt_Address);
        btn_locationChange = view.findViewById(R.id.btn_locationChange);
        mBottomSheetBehavior1 = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_COLLAPSED);
        mBottomSheetBehavior1.setPeekHeight(0);
        rvOrderData.setOnTouchListener(new RecyclerView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_EXPANDED);
                        break;

                    case MotionEvent.ACTION_UP:
                        mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_EXPANDED);
                        break;
                }

                // Handle RecyclerView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
        return view;
    }

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            //your code here
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };

    @SuppressLint("SetTextI18n")
    public void AddToCartData() {
        /*
         * Add To Cart bottom sheet Data
         * */

        if (addtoCartDataList.size() > 0) {
            AddToCartDataAdapter addToCartDataAdapter = new AddToCartDataAdapter(getContext(), addtoCartDataList);
            RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            rvOrderData.setLayoutManager(linearLayoutManager);
            rvOrderData.setItemAnimator(new DefaultItemAnimator());
            rvOrderData.setAdapter(addToCartDataAdapter);
            double totalPrice = 0;
            for (int i = 0; i < addtoCartDataList.size(); i++) {
                for (int j = 0; j < addtoCartDataList.get(i).getStoreProductData().getVariantName().size(); j++) {
                    totalPrice += addtoCartDataList.get(i).getStoreProductData().getVariantName().get(j).getProductPrice();
                }
            }
            totalAmount = totalPrice + 45;
            txtTotalPrice.setText("Total Amount : " + String.valueOf(totalAmount) + "RS");

        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnViewCart) {
            if (mBottomSheetBehavior1.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_EXPANDED);
                AddToCartData();
                LocationTrack locationTrack = new LocationTrack(getActivity());

                if (locationTrack.canGetLocation()) {
                    double longitude = locationTrack.getLongitude();
                    double latitude = locationTrack.getLatitude();
                    Log.d("TAG", "onCreateView: lat : " + longitude);
                    Log.d("TAG", "onCreateView: long : " + latitude);
                    Geocoder geocoder;
                    List<Address> addresses = null;
                    geocoder = new Geocoder(getContext(), Locale.getDefault());

                    try {
                        addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (addresses.size() > 0) {
                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        txt_currentAddress.setText(address);
                        //  }
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    myCondition = true;
                } else {
                    myCondition = true;
                    mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    mBottomSheetBehavior1.setPeekHeight(0);
                }
            }
        }
        if (v.getId() == R.id.btnPlaceOrder) {
            PlaceOrder();
        }
        if (v.getId() == R.id.btn_locationChange) {
            Intent intent = new Intent(getActivity(), AddressBookActivity.class).putExtra("FromProfile", false);
            startActivity(intent);

        }
    }

    @Override
    public boolean onBackPressed() {
        if (myCondition) {
            Toast.makeText(getContext(), "Expand", Toast.LENGTH_SHORT).show();
            mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_COLLAPSED);
            mBottomSheetBehavior1.setPeekHeight(0);
            myCondition = false;
            return false;
        } else {
            getActivity().finish();
            return true;
        }
    }

    private void PlaceOrder() {
        Map<String, String> params = new HashMap<>();
        for (int i = 0; i < placeOrderModels.size(); i++) {
            params.put("sp_id", placeOrderModels.get(i).getSpId());
            params.put("product_id", placeOrderModels.get(i).getProductId());
            params.put("quantity", String.valueOf(placeOrderModels.get(i).getQuantity()));
            params.put("product_price", String.valueOf(placeOrderModels.get(i).getVariantPrice()));
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            try {
                ApiInstant service = BaseRetrofit.getRetrofitInstance().create(ApiInstant.class);
                Call<PlaceOrderResponse> call = service.PlaceOrder(phoneNumber, android_device_id, Store_ID, category,
                        1, "Cash", totalAmount, 0, 45, "ABD50", "Instant", params);
                call.enqueue(new Callback<PlaceOrderResponse>() {
                    @Override
                    public void onResponse(Call<PlaceOrderResponse> call, Response<PlaceOrderResponse> response) {
                        if (response.isSuccessful()) {
                            btnPlaceOrder.setText("Done");
                            Intent intent = new Intent(getContext(), DrawerProfileActivity.class);
                            startActivity(intent);
                            Toast.makeText(getContext(), "Order placed Successfully!", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }


                    @Override
                    public void onFailure(Call<PlaceOrderResponse> call, Throwable t) {
                        Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getContext(), "Something went to wrong.", Toast.LENGTH_SHORT).show();
        }
    }
}