package com.viamart.customer.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.JsonObject;
import com.viamart.customer.R;
import com.viamart.customer.adapter.ShopeDetailsViewPager;
import com.viamart.customer.base_util_premission.BaseActivity;
import com.viamart.customer.model.StoreCategory;
import com.viamart.customer.retrofit.ApiInstant;
import com.viamart.customer.retrofit.BaseRetrofit;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopeDetailsFragment extends BaseActivity implements TabLayout.OnTabSelectedListener, View.OnClickListener {


    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.imgBack)
    AppCompatImageView imgBack;
    @BindView(R.id.txtShopeName)
    AppCompatTextView txtShopeName;
    @BindView(R.id.imgStoreBg)
    AppCompatImageView imgStoreBg;
    @BindView(R.id.txtkm)
    AppCompatTextView txtkm;
    @BindView(R.id.txtCityName)
    AppCompatTextView txtCityName;
    @BindView(R.id.txttime)
    AppCompatTextView txttime;
    private int category_id, value;
    private ShimmerFrameLayout shimmerFrameLayout;
    private SharedPreferences sh;
    private List<StoreCategory.ProductCategory> datumList = new ArrayList<>();
    String tokenValue;
    List<StoreCategory.Product> productList= new ArrayList<>();
    ShopeDetailsViewPager shopeDetailsViewPager;
    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        //tabLayout.getTabAt(tab.getPosition()).select();
        viewPager.setCurrentItem(tab.getPosition());
        productList= null;
        category_id = datumList.get(tab.getPosition()).getCategoryId();
        storeDetails(value, String.valueOf(category_id));
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_shope_details);
        ButterKnife.bind(this);
        sh = getSharedPreferences("MySharedPref", Context.MODE_APPEND);
        tokenValue = sh.getString("access_token", "");
    /*}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_shope_details, container, false);
        ButterKnife.bind(this, view);*/
        shimmerFrameLayout = findViewById(R.id.shimmerEffectGrocery);
        shimmerFrameLayout.startShimmer();
        shimmerFrameLayout.setVisibility(View.VISIBLE);
        imgBack.setOnClickListener(this);

        String city = getIntent().getExtras().getString("city");
        String storeName = getIntent().getExtras().getString("storeName");
        String distance = getIntent().getExtras().getString("distance");
        value = getIntent().getExtras().getInt("storeId");
        String img = getIntent().getExtras().getString("img");
        String duration = getIntent().getExtras().getString("duration");
        txtCityName.setText(city);
        txttime.setText(duration);
        txtShopeName.setText(storeName);
        txtkm.setText(distance);
        storeCategory(value);

        if (img != null) {
            Glide.with(getApplicationContext()).
                    load(img).
                    into(imgStoreBg);
        }
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        //Creating our pager adapter

        tabLayout.setOnTabSelectedListener(this);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tabLayout.getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        // return;
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imgBack) {
            onBackPressed();
        }
    }

   /* @Override
    public boolean onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
        return true;
    }*/

    private void storeCategory(int value) {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            try {

                JsonObject obj = new JsonObject();
                obj.addProperty("method", "getStoreDetails");
                JsonObject payerReg = new JsonObject();
                payerReg.addProperty("store_id", String.valueOf(value));
                payerReg.addProperty("access_token", tokenValue);
                obj.add("body", payerReg);


                ApiInstant service = BaseRetrofit.getRetrofitInstance().create(ApiInstant.class);
                Call<StoreCategory> call = service.getStoreCategories(obj);
                call.enqueue(new Callback<StoreCategory>() {
                    @Override
                    public void onResponse(Call<StoreCategory> call, Response<StoreCategory> response) {

                        if (response.isSuccessful()) {
                            StoreCategory store = response.body();
                            datumList = store.getGetStoreDetails().getData().getProductCategory();
                            //Adding onTabSelectedListener to swipe views
                            for (int i = 0; i < datumList.size(); i++) {
                                tabLayout.addTab(tabLayout.newTab().setText(String.valueOf(datumList.get(i).getCategoryName())));
                                category_id = datumList.get(i).getCategoryId();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Something went wrong...Please try", Toast.LENGTH_SHORT).show();
                        }
                    }


                    @Override
                    public void onFailure(Call<StoreCategory> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Somthing went to wrong.", Toast.LENGTH_SHORT).show();
        }
    }

    private void storeDetails(int Store_ID, String category_id) {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            try {
                //shopeDetailsViewPager.
                viewPager.setVisibility(View.GONE);
                shimmerFrameLayout.startShimmer();
                shimmerFrameLayout.setVisibility(View.VISIBLE);
                JsonObject obj = new JsonObject();
                obj.addProperty("method", "getStoreDetails");
                JsonObject payerReg = new JsonObject();
                payerReg.addProperty("store_id", String.valueOf(value));
                payerReg.addProperty("category_id", category_id);
                payerReg.addProperty("access_token", tokenValue);
                obj.add("body", payerReg);

                ApiInstant service = BaseRetrofit.getRetrofitInstance().create(ApiInstant.class);
                Call<StoreCategory> call = service.StoreProductDat(obj);
                call.enqueue(new Callback<StoreCategory>() {
                    @Override
                    public void onResponse(Call<StoreCategory> call, Response<StoreCategory> response) {
                        if (response.isSuccessful()) {
                            viewPager.setVisibility(View.VISIBLE);
                            shimmerFrameLayout.stopShimmer();
                            shimmerFrameLayout.setVisibility(View.GONE);
                            if(response.body() != null){
                            StoreCategory store = response.body();
                            // txtShopeName.setText(store.getStoreName());

                            if(store.getGetStoreDetails().getData().getProducts() != null){
                                productList = store.getGetStoreDetails().getData().getProducts();

                             shopeDetailsViewPager = new ShopeDetailsViewPager(getApplicationContext(),Store_ID, category_id, productList, getSupportFragmentManager(), tabLayout.getTabCount());

                            //Adding adapter to pager
                            viewPager.setAdapter(shopeDetailsViewPager);}}
                        } else {
                            Toast.makeText(getApplicationContext(), "Something went wrong...Please try", Toast.LENGTH_SHORT).show();
                        }
                    }


                    @Override
                    public void onFailure(Call<StoreCategory> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Somthing went to wrong.", Toast.LENGTH_SHORT).show();
        }
    }
}