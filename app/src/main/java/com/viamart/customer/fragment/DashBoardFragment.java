package com.viamart.customer.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.JsonObject;
import com.viamart.customer.R;
import com.viamart.customer.activity.CartLIstActivity;
import com.viamart.customer.adapter.FilterCategoryAdapter;
import com.viamart.customer.adapter.HeaderOfferViewPager;
import com.viamart.customer.adapter.InstructionAdapter;
import com.viamart.customer.adapter.ShopDataAdapter;
import com.viamart.customer.adapter.SpecialShopOffer;
import com.viamart.customer.interfaceData.IOnBackPressed;
import com.viamart.customer.model.SetLatLong;
import com.viamart.customer.model.StoreCategoryFilter;
import com.viamart.customer.model.StoreDetails;
import com.viamart.customer.retrofit.ApiInstant;
import com.viamart.customer.retrofit.BaseRetrofit;
import com.viamart.customer.utils.LocationTrack;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.LOCATION_SERVICE;


public class DashBoardFragment extends Fragment implements View.OnClickListener, IOnBackPressed {
    @BindView(R.id.rv_instruction)
    RecyclerView rv_instruction;
    @BindView(R.id.offer_img)
    ViewPager offer_img;
    AppCompatTextView addres_location;
    @BindView(R.id.rv_main_offer)
    RecyclerView rv_main_offer;
    @BindView(R.id.rv_shope_Data)
    RecyclerView rv_shope_Data;
    @BindView(R.id.txt_inst_offer)
    AppCompatTextView txtFilter;
    private int currentPage = 0;
    private static int NUM_PAGES = 4;
    //private BottomSheetBehavior sheetBehavior;
  //  private LinearLayout filter_bottom_sheet;
    /*@BindView(R.id.rvFilterData)
    RecyclerView rvFilterData;*/
    private boolean myCondition;
  /*  @BindView(R.id.btnApplyCategory)
    AppCompatButton btnApplyCategory;*/
    @BindView(R.id.imgCart)
    AppCompatImageView imgCart;
    private Menu menu;
    private ShimmerFrameLayout shimmerFrameLayout;
    private SharedPreferences sh;
    private String phoneNumber, android_device_id;
    private double latitude, longitude;
    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();
    private final static int ALL_PERMISSIONS_RESULT = 101;
    private String tokenValue;

    public DashBoardFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @SuppressLint({"WrongConstant", "ClickableViewAccessibility"})
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_dashboard, container, false);
        ButterKnife.bind(this, view);


        NestedScrollView nestesScrollw = view.findViewById(R.id.nestesScrollw);
        addres_location = view.findViewById(R.id.addres_location_header);
        shimmerFrameLayout = view.findViewById(R.id.shimmerEffect);
        shimmerFrameLayout.startShimmer();
        shimmerFrameLayout.setVisibility(View.VISIBLE);
        sh = getActivity().getSharedPreferences("MySharedPref", Context.MODE_APPEND);
        phoneNumber = sh.getString("phoneNumber", "");
        android_device_id = sh.getString("Device_ID", "");
        tokenValue = sh.getString("access_token", "");
        imgCart.setOnClickListener(this);
        LocationTrack locationTrack = new LocationTrack(getActivity());
        permissionsToRequest = findUnAskedPermissions(permissions);
        if (locationTrack.canGetLocation()) {
            longitude = locationTrack.getLongitude();
            latitude = locationTrack.getLatitude();
            Geocoder geocoder;
            List<Address> addresses = null;
            geocoder = new Geocoder(getActivity(), Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                if (addresses.size() > 0) {
                    if (addresses.get(0).getLocality() != null) {
                        String city = addresses.get(0).getLocality();
                        addres_location.setText(city);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            locationTrack.showSettingsAlert();
        }
       // filter_bottom_sheet = view.findViewById(R.id.filter_bottom_sheet);

        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {

            storeData(tokenValue, longitude, latitude, phoneNumber, android_device_id);
            storeCategoryData();
        } else {
            Toast.makeText(getContext(), "Check your Inter connection.", Toast.LENGTH_SHORT).show();
        }
        LocationManager mLocationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1,
                    1, mLocationListener);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    1);
        }


        //sheetBehavior = BottomSheetBehavior.from(filter_bottom_sheet);
        ///sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
       // sheetBehavior.setPeekHeight(0);
        HeaderOfferViewPager headerOfferViewPager = new HeaderOfferViewPager(getContext());
        offer_img.setAdapter(headerOfferViewPager);
        txtFilter.setOnClickListener(this);
    //    btnApplyCategory.setOnClickListener(this);
        AppBarLayout mAppBarLayout = (AppBarLayout) view.findViewById(R.id.appBarLayout);
        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true;
                    //showOption(R.id.action_info);
                } else if (isShow) {
                    isShow = false;
                    // hideOption(R.id.action_info);
                }
            }
        });


        /*
         * Bottom Sheet Data
         *
         * */
       /* FilterCategoryAdapter filterCategoryAdapter = new FilterCategoryAdapter(getActivity());
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1);
        rvFilterData.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
        rvFilterData.setAdapter(filterCategoryAdapter);*/

        /*After setting the adapter use the timer */
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                offer_img.setCurrentItem(currentPage++, true);
            }
        };

        Timer timer = new Timer(); // This will create a new Thread
        long DELAY_MS = 500;
        long PERIOD_MS = 3000;
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);


        return view;
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.txt_inst_offer) {
           //OpenFilterData();
        }
        /*if (v.getId() == R.id.btnApplyCategory) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            sheetBehavior.setPeekHeight(0);
        }*/
        if (v.getId() == R.id.imgCart) {
            OpenCartList();
        }
    }

    private void OpenFilterData() {

       /* if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            //sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            myCondition = true;
            //filter_bottom_sheet.setVisibility(View.VISIBLE);
        } else {
            myCondition = false;
          //  sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
          //  sheetBehavior.setPeekHeight(0);
          //  filter_bottom_sheet.setVisibility(View.GONE);
        }*/
    }

    @Override
    public boolean onBackPressed() {
        if (myCondition) {
            Toast.makeText(getContext(), "Expand", Toast.LENGTH_SHORT).show();
           // sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            //filter_bottom_sheet.setVisibility(View.GONE);
          //  sheetBehavior.setPeekHeight(0);
            myCondition = false;
            return false;
        } else {
            getActivity().finish();
            return true;
        }
    }

    private void storeData(String tokenValue, double longitude, double latitude, String phoneNumber, String android_device_id) {
        /*Create handle for the RetrofitInstance interface*/
        try {
            JsonObject obj = new JsonObject();
            obj.addProperty("method", "getStores");
            JsonObject payerReg = new JsonObject();
            payerReg.addProperty("access_token", tokenValue);
            payerReg.addProperty("customer_phone", phoneNumber);
            payerReg.addProperty("lati", latitude);
            payerReg.addProperty("longi", longitude);
            payerReg.addProperty("device_uid", android_device_id);
            obj.add("body", payerReg);
            ApiInstant service = BaseRetrofit.getRetrofitInstance().create(ApiInstant.class);
            Call<StoreDetails> call = service.storeList(obj);
            call.enqueue(new Callback<StoreDetails>() {
                @Override
                public void onResponse(Call<StoreDetails> call, Response<StoreDetails> response) {
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        StoreDetails store = response.body();
                        List<StoreDetails.CategoryDatum> categoryData = store.getGetStores().getData().getCategoryData();
                        List<StoreDetails.StoreDetail> datumList = store.getGetStores().getData().getStoreDetails();

                        /*
                         * Defalut Shop List
                         * */
                        ShopDataAdapter shopDataAdapter = new ShopDataAdapter(datumList, getContext(), new ShopDataAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                int storeId = Integer.parseInt(datumList.get(position).getStoreId());
                                String storeName = datumList.get(position).getStoreName();
                                String distance = datumList.get(position).getDistance();
                                String img = datumList.get(position).getStoreImage();
                                String city = datumList.get(position).getCityName();
                                String duration = datumList.get(position).getDuration();
                                Log.d("TAG", "onItemClick: " + storeId);
                                Intent intent = new Intent(getContext(), ShopeDetailsFragment.class);
                                Bundle bundle = new Bundle();
                                bundle.putInt("storeId", storeId);
                                bundle.putString("city", city);
                                bundle.putString("storeName", storeName);
                                bundle.putString("distance", distance);
                                bundle.putString("img", img);
                                bundle.putString("duration", duration);
                                intent.putExtras(bundle);
                                startActivity(intent);
                            }
                        });
                        RecyclerView.LayoutManager mLayoutManagershopName = new LinearLayoutManager(getContext());
                        rv_shope_Data.setLayoutManager(mLayoutManagershopName);
                        rv_shope_Data.setItemAnimator(new DefaultItemAnimator());
                        rv_shope_Data.setAdapter(shopDataAdapter);


                        /*
                         *
                         * Special Shop offer
                         * */
                        SpecialShopOffer specialShopOffer = new SpecialShopOffer(categoryData, getActivity(), new SpecialShopOffer.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                              //  filter_bottom_sheet.setVisibility(View.GONE);
                                /* OfferShopeListFragment nextFrag = new OfferShopeListFragment();
                                getActivity().getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.frameLayout, nextFrag, "findThisFragment")
                                        .addToBackStack(null)
                                        .commit();*/

                                /*
                                 * Category wise shop list
                                 * */
                                int categoryId = categoryData.get(position).getCategoryId();
                                getStoreListCategoryWise(categoryId);
                            }
                        });
                        GridLayoutManager mLayoutManagershopOffer = new GridLayoutManager(getContext(), 4);
                        rv_main_offer.setLayoutManager(mLayoutManagershopOffer);
                        rv_main_offer.setItemAnimator(new DefaultItemAnimator());
                        rv_main_offer.setAdapter(specialShopOffer);


                    } else {
                        Toast.makeText(getContext(), "Something went wrong...Please try", Toast.LENGTH_SHORT).show();
                    }
                }


                @Override
                public void onFailure(Call<StoreDetails> call, Throwable t) {
                    shimmerFrameLayout.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void storeCategoryData() {
        /*Create handle for the RetrofitInstance interface*/
        try {
            ApiInstant service = BaseRetrofit.getRetrofitInstance().create(ApiInstant.class);
            Call<StoreCategoryFilter> call = service.StoreCategoryFilter();
            call.enqueue(new Callback<StoreCategoryFilter>() {
                @Override
                public void onResponse(Call<StoreCategoryFilter> call, Response<StoreCategoryFilter> response) {
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        StoreCategoryFilter store = response.body();
                        List<StoreCategoryFilter.Datum> datumList = store.getData();
                        /*
                         * Instruction Data  (HORIZONTAL View)
                         * */
                        InstructionAdapter instructionAdapter = new InstructionAdapter(datumList, getContext());
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(),
                                LinearLayoutManager.HORIZONTAL,
                                false);
                        rv_instruction.setLayoutManager(mLayoutManager);
                        rv_instruction.setItemAnimator(new DefaultItemAnimator());
                        rv_instruction.setAdapter(instructionAdapter);
                    } else {
                        //  Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                    }
                }


                @Override
                public void onFailure(Call<StoreCategoryFilter> call, Throwable t) {
                    shimmerFrameLayout.setVisibility(View.GONE);
                    Toast.makeText(getContext(), call.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

     private void getStoreListCategoryWise(int categoryID) {
        try {
            JsonObject obj = new JsonObject();
            obj.addProperty("method", "getStores");
            JsonObject payerReg = new JsonObject();
            payerReg.addProperty("access_token", tokenValue);
            payerReg.addProperty("category_id", categoryID);
            /*payerReg.addProperty("lati", latitude);
            payerReg.addProperty("longi", longitude);
            payerReg.addProperty("device_uid", android_device_id);*/
            obj.add("body", payerReg);
            ApiInstant service = BaseRetrofit.getRetrofitInstance().create(ApiInstant.class);
            Call<StoreDetails> call = service.getStoreListCategoryWIse(obj);
            call.enqueue(new Callback<StoreDetails>() {
                @Override
                public void onResponse(Call<StoreDetails> call, Response<StoreDetails> response) {
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        StoreDetails store = response.body();
                        List<StoreDetails.StoreDetail> datumList = store.getGetStores().getData().getStoreDetails();
                        ShopDataAdapter shopDataAdapter = new ShopDataAdapter(datumList, getContext(), new ShopDataAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                int storeId = Integer.parseInt(datumList.get(position).getStoreId());
                                String storeName = datumList.get(position).getStoreName();
                                String distance = datumList.get(position).getDistance();
                                String img = datumList.get(position).getStoreImage();
                                String city = datumList.get(position).getCityName();
                                String duration = datumList.get(position).getDuration();
                                Log.d("TAG", "onItemClick: " + storeId);
                                Intent intent = new Intent(getContext(), ShopeDetailsFragment.class);
                                Bundle bundle = new Bundle();
                                bundle.putInt("storeId", storeId);
                                bundle.putString("city", city);
                                bundle.putString("storeName", storeName);
                                bundle.putString("distance", distance);
                                bundle.putString("img", img);
                                bundle.putString("duration", duration);
                                intent.putExtras(bundle);
                                startActivity(intent);
                            }
                        });
                        RecyclerView.LayoutManager mLayoutManagershopName = new LinearLayoutManager(getContext());
                        rv_shope_Data.setLayoutManager(mLayoutManagershopName);
                        rv_shope_Data.setItemAnimator(new DefaultItemAnimator());
                        rv_shope_Data.setAdapter(shopDataAdapter);

                    } else {
                        Toast.makeText(getContext(), "Something went wrong...Please try", Toast.LENGTH_SHORT).show();
                    }
                }


                @Override
                public void onFailure(Call<StoreDetails> call, Throwable t) {
                    shimmerFrameLayout.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            //your code here
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();

          /*  Geocoder geocoder;
            List<Address> addresses = null;
            geocoder = new Geocoder(getActivity(), Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } catch (IOException e) {
                e.printStackTrace();
            }

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();
            addres_location.setText(city);*/
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }


        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();

        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (getActivity().checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (Object perms : permissionsToRequest) {
                    if (!hasPermission((String) perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions((String[]) permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void OpenCartList() {
        Intent intent = new Intent(getActivity(), CartLIstActivity.class);
        startActivity(intent);
    }


}