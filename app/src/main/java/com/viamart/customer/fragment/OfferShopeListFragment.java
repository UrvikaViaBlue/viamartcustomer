package com.viamart.customer.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.viamart.customer.R;
import com.viamart.customer.adapter.OfferShopeListAdapter;
import com.viamart.customer.interfaceData.IOnBackPressed;

import butterknife.BindView;
import butterknife.ButterKnife;


public class OfferShopeListFragment extends Fragment implements IOnBackPressed {

    @BindView(R.id.rv_shope_list)
    RecyclerView rv_shope_list;
    OfferShopeListAdapter offerShopeListAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_offer_shope_list, container, false);
        ButterKnife.bind(this, view);
        offerShopeListAdapter = new OfferShopeListAdapter(getActivity(), new OfferShopeListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Log.d("TAG", "onItemClick: data : ");
                /*ShopeDetailsFragment nextFrag = new ShopeDetailsFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frameLayout, nextFrag, "findThisFragment")
                        .addToBackStack(null)
                        .commit();*/
            }
        });
        RecyclerView.LayoutManager mLayoutManagershopName = new LinearLayoutManager(getContext());
        rv_shope_list.setLayoutManager(mLayoutManagershopName);
        rv_shope_list.setItemAnimator(new DefaultItemAnimator());
        rv_shope_list.setAdapter(offerShopeListAdapter);
        return view;

    }

    @Override
    public boolean onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
        return false;
    }
}