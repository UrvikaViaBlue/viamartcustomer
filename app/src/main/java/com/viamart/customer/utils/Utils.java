package com.viamart.customer.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Window;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by HIren on 13-12-2016.
 */

public class Utils {

    public static boolean isOnline(Context mContext) {

        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        boolean is = (netInfo != null && netInfo.isConnectedOrConnecting());
        return is;
    }

    public static boolean isEmpty(String text) {

        if (text != null && !text.equalsIgnoreCase("null") && !text.trim().equals("")) {
            return false;
        }
        return true;
    }

    public static boolean isValidPhone(String phone) {

        return phone.length() > 7;
    }

    public static boolean isValidPassword(String phone) {

        return phone.length() > 5;
    }

    public static boolean isValidEmail(String email) {


        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static Dialog progressdialog(Context context) {


        Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //  dialog.setContentView(R.layout.loading_layout);
        dialog.setCancelable(false);

        return dialog;
    }

    public static boolean isValidZip(String ZipCode) {
        return ZipCode.length() < 12;
    }

}
