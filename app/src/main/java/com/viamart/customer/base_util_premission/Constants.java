package com.viamart.customer.base_util_premission;

import android.graphics.Color;

public class Constants {

    public final static int TAG_SPLASH_SCREEN_TIME = 2000;


    /* ***************************************
     *         TAG / LABEL NAME AND VALUE           *
     ***************************************/
    public static final String TAG_NO_INTERNET_CONNECTION = "No Internet Connection!";
    public static final String TAG_NO_INTERNET_CONNECTION_MESSAGE = "It seems you are offline.Check your network setting and try again.";
    public static final String TAG_ERROR_PERMISSION = "Storage write permission is needed to save the files.";
    public static final String TAG_RETRY = "Retry";
    public static final String TAG_PLEASE_WAIT = "Please wait..";
    public static final String TAG_OK = "OK";
    public static final String TAG_ADD = "Add";
    public static final String TAG_ISSUE_REFUND = "Issue Refund";
    public static final String TAG_SUBMIT = "Submit";
    public static final String TAG_YES = "Yes";
    public static final String TAG_Cancel = "Cancel";
    public static final String TAG_NO = "No";
    public static final String TAG_PLEASE_TRY_AGAIN = "Please try again , Some time later.";
    public static final String TAG_SOMETHING_WENT_WRONG = "Something went wrong.";
    public static final String TAG_DATA_SYNC_SUCCESSFULLY = "Data synced successfully!";
    public static final String TAG_ORDER_REFUND_ALL = "Already Refunded.";
    public static final String TAG_ORDER_REFUND_UPDATE = "Return order update success!";
    public static final String TAG_Event_SYNC_SUCCESSFULLY = "Event synced successfully!";
    public static final String TAG_DATA_Not_SYNC = "Sync is not successful please try again...";
    public static final String TAG_APPLY_DISCOUNT = "Apply Discount";
    public static final String TAG_Coming_Soon = "Coming Soon...";
    /**
     * SIZE FRAGMENT LABLE
     */
    public static final String TAG_INVALID_SIZE_QUANTITY = "Please enter quantity for product size you want.";
    public static final String TAG_NO_DESIGN_AVALIABLE = "No Design Available ";

    public static final String TAD_DESIGN_NO_SELECTED = "Please select design(s) to move further.";

    /* ***************************************
     *         COLOR USE                    *
     ***************************************/
    public static final int[] mColors_Icon = {Color.parseColor("#63FF9C"),
            Color.parseColor("#50CE7E"),
            Color.parseColor("#F44336"),
            Color.parseColor("#FF9800"),
            Color.parseColor("#FF4B54"),
            Color.parseColor("#31363B"),
            Color.parseColor("#2196F3"),
            Color.parseColor("#FDFD1D"),
            Color.parseColor("#3D89FD"),
            Color.parseColor("#FD7178")};

    public static final int[] mColors = {Color.parseColor("#000000"), // RED
            Color.parseColor("#000000"),
            Color.parseColor("#000000"),
            Color.parseColor("#000000"),
            Color.parseColor("#000000"),
            Color.parseColor("#000000"),
            Color.parseColor("#000000"),
            Color.parseColor("#000000"),
            Color.parseColor("#000000"),
            Color.parseColor("#000000")};


    /* ***************************************
     *         UNIQUE KEY NAME              *
     ***************************************/
    public final static String KEY_SUCCESS = "Success";
    public final static String KEY_Waring = "Waring";
    public final static String KEY_MESSAGE = "Message";
    public final static String KEY_RESULT_MODEL = "resultModel";
    public final static String KEY_PASSCODE = "passcode";
    public final static String KEY_DEVICEID = "deviceId";
    public final static String KEY_DeviceId = "DeviceId";
    public final static String KEY_EventId = "eventId";
    public final static String KEY_JSON_EVENTS = "Events";
    public final static String KEY_JSON_VENUES = "Venues";
    public final static String KEY_JSON_EMPLOYEE = "Employees";
    public final static String KEY_JSON_PRODUCTS = "Products";
    public final static String KEY_JSON_PRODUCTS_BY_VENUE = "ProductsByVenue";
    public final static String KEY_JSON_COLOR = "Colors";
    public final static String KEY_JSON_SIZE = "Sizes";
    public final static String KEY_JSON_PCSPQ = "ProductColorSizePriceQty";
    public final static String KEY_JSON_Location = "Locations";
    public final static String KEY_JSON_COUNTRY = "Countries";
    public final static String KEY_JSON_StateProvinces = "StateProvinces";
    public final static String KEY_JSON_Cities = "Cities";
    public final static String KEY_JSON_LEADS = "Leads";
    public final static String KEY_JSON_EVENT_DESIGN = "EventDesigns";
    public final static String KEY_JSON_EVENT_DESIGN_BY_VENUE_ID = "EventDesignsByVenue";
    public final static String KEY_ACTIVITY_TITLE_LEADSLIST = "Leads List";
    public final static String KEY_JSON_CONFIGURATIONS = "Configurations";
    public final static String KEY_JSON_SETTING = "Settings";
    public final static String KEY_JSON_CURRENCIES = "Currencies";
    public final static String KEY_JSON_TAXRATE = "TaxRates";
    public final static String KEY_JSON_SUPPLIRES = "Suppliers";
    public final static String KEY_JSON_PURCHASE_ORDER = "PurchaseOrders";
    public final static String KEY_JSON_PURCHASE_ORDER_ITEM = "PurchaseOrderItems";
    public final static String KEY_JSON_PURCHASE_ORDER_ITEM_SQ = "PurchaseOrderItemSizeQuantities";
    public final static String KEY_JSON_PURCHASE_ORDER_ITEM_DELIVERIES = "PurchaseOrderItemSizeDeliveries";

    /***********************************************************************************************
     * FINAL JSON MAIN OBJECT
     ***********************************************************************************************/
    public final static String KEY_ACTIVITY_TITLE_EVENTS = "Events";
    public final static String KEY_ACTIVITY_TITLE_VENUES = "Venues";
    public final static String KEY_ACTIVITY_TITLE_EMPLOYEE = "Employees";
    public final static String KEY_ACTIVITY_TITLE_DASHBOARD = "Dashboard";
    public final static String KEY_ACTIVITY_TITLE_CART_LIST = "My Cart";
    public final static String KEY_ACTIVITY_TITLE_PAYMENT = "Payment";
    public final static String KEY_ACTIVITY_TITLE_OrderList = "Order List";
    public final static String KEY_ACTIVITY_TITLE_OrderDetails = "Order Details";
    /***********************************************************************************************
     * FINAL JSON MAIN OBJECT
     ***********************************************************************************************/
    public final static String KEY_ORDER_SESSION_EVENTS = "Events";
    public final static String KEY_ORDER_SESSION_VENUES = "Venues";
    public final static String KEY_ORDER_SESSION_EMPLOYEE = "Employee";
    public final static String KEY_ORDER_SESSION_START = "SessionStart";
    public final static String KEY_ORDER_SESSION_ORDER_ITEM = "OrderItem";
    public final static String KEY_ORDER_SESSION_PRODUCT_ITEM = "ProductItem";

    public final static String KEY_ORDER_SESSION_PRODUCT_ITEM_LISt = "ProductItem";
    /***********************************************************************************************
     * FINAL ORDER KEY
     ***********************************************************************************************/
    public final static String KEY_ORDER_GU_ID = "OrderGuid";
    public final static String KEY_ORDER_ITEMS_GU_ID = "OrderItemGuid";
    public final static String KEY_ORDER_ITEMS_PRODUCT_ID = Constants.KEY_PRODUCT_ID;
    public final static String KEY_ORDER_ITEMS_QUANTITY = "Quantity";
    public final static String KEY_ORDER_ITEMS_PRICE_INC_TEXT = "PriceInclTax";
    public final static String KEY_ORDER_ITEMS_PRICE_EXCL_TEXT = "PriceExclTax";
    public final static String KEY_ORDER_ITEMS_DICOUNT_AMOUNT_INC_TEXT = "DiscountAmountInclTax";
    public final static String KEY_ORDER_ITEMS_DICOUNT_AMOUNT_EXCL_TEXT = "DiscountAmountExclTax";
    public final static String KEY_ORDER_ITEMS_DICOUNT_PERCENTAGE = "DiscountPercentage";
    public final static String KEY_ORDER_ITEMS_COLOR_ID = "ColorId";
    public final static String KEY_ORDER_ITEMS_SIZE_QUANTITIES = "OrderItemSizeQuantities";
    public final static String KEY_ORDER_ITEMS_DESING = "OrderItemDesigns";

    /***********************************************************************************************
     * INTENT_ORDER_DATA_MODEL
     ***********************************************************************************************/
    public static final String KEY_INTENT_ORDER_DATA_MODEL = "OrderSessionModel";

    /***********************************************************************************************
     * SHARE PREFERENCES FILE NAME
     ***********************************************************************************************/
    // SHARE PREFERENCES NAME AND INSERTED VALUE
    public static final String PREFERENCE_USER = "POSPreferences";
    public static final String PREFERENCE_USER_MODEL = "UserModel";
    public static final String PREFERENCE_ORDER_SEASSION_NAME = "OrderSession";
    public static final String PREFERENCE_ORDER_DATA_MODEL = "OrderDataModel";
    public static final int PRIVATE_MODE = 0;

    public static final String PREFERENCE_CURRENCIES_KEY = "Currencies";
    public static final String PREFERENCE_CURRENCIES_MODEL = "CurrenciesModel";

    public static final String PREFERENCE_DISCOUNT = "DISCOUNT";
    public static final String PREFERENCE_DISCOUNT_PSW = "DISCOUNTPSW";

    /***********************************************************************************************
     * USER SESSION MODEL
     ***********************************************************************************************/
    // LOGIN USER SESSION MANAGEMENT KEY AND VALUE
    public static final String KEY_USER_PASSCODE = Constants.KEY_PASSCODE;
    public static final String KEY_DEVICE_ID = "DeviceId";
    public static final String KEY_USER_CHECK_LOGIN = "IsLogin";
    public static final String KEY_Event_ID = Constants.KEY_EventId;

    /***********************************************************************************************
     * STATIC FORMAT DATE > TIME >
     ***********************************************************************************************/
    // EVENTS START DATE AND END DATE INPUT FORMAT
    public static final String KEY_DATE_FORMAT_OUTPUT = "dd-MMM-yyyy";
    public static final String KEY_DATE_FORMAT_INPUT = "yyyy-MM-dd'T'HH:mm:ss";

    /***********************************************************************************************
     * SD CARD STORAGE FOLDER STRUCTURE
     ***********************************************************************************************/
    // EVENTS IMAGE DOWNLOAD FOLDER NAME
    public static final String KEY_STORAGE_FOLDER_EVENT_IMAGE = ".EventsImage";
    public static final String KEY_STORAGE_FOLDER_PRODUCT_IMAGE = ".ProductsImage";
    public static final String KEY_STORAGE_FOLDER_DESING_IMAGE = ".DesignsImage";
    public static final String KEY_STORAGE_FOLDER_LEADS_IMAGE = ".Leads";


    /***********************************************************************************************
     * LOCAL DATABASE KEY AND FIELD NAME
     ***********************************************************************************************/
    // DATABASE NAME AND VERSION
    public static final String DB_NAME = "POSDatabase";

    //Suppliers TABLE
    public static final String KEY_ID = "Id";
    public static final String KEY_NAME = "Name";

    //PURCHASE ORDER TABLE
    public static final String KEY_PO_DATE = "PODate";
    public static final String KEY_SUPPLIER_ID = "SupplierId";

    //PURCHASE ORDER ITEM TABLE
    public static final String KEY_PURCHASE_ORDER_ID = "PurchaseOrderId";


    // PURCHASE ORDER ITEM SIZE QUANTITY MODEL
    public static final String KEY_PRODUCT_ORDER_ITEM_ID = "PurchaseOrderItemId";
    public static final String KEY_SIZE_ID_CAPITAL = "SizeID";
    public static final String KEY_ORDERED_QTY = "OrderedQty";
    public static final String KEY_P_O_ITEM_SIZE_QUANTITIES_ID = "PurchaseOrderItemSizeQuantitiesId";
    public static final String KEY_QTY_DELIVERED = "QtyDelivered";


    // EVENTS TABLE
    public static final String KEY_EVENT_ID = "EventId";
    public static final String KEY_EVENT_NAME = "EventName";
    public static final String KEY_EVENT_CUSTOMER = "Customer";
    public static final String KEY_EVENT_ORGANIZATION = "Organization";
    public static final String KEY_EVENT_LOGO = "EventLogoURL";
    public static final String KEY_EVENT_START_DATE = "EventStartDate";
    public static final String KEY_EVENT_END_DATE = "EventEndDate";
    public static final String KEY_EVENT_IsFirstDesign_Free = "IsFirstDesignFree";
    public static final String KEY_EVENT_FirstDesign_Price = "FirstDesignPrice";

    // VENUES TABLE
    public static final String KEY_VENUES_ID = "VenueId";
    public static final String KEY_VENUES_NAME = "VenueName";

    // EMPLOYEE TABLE
    public static final String KEY_EMPLOYEE_ID = "EmployeeId";
    public static final String KEY_EMPLOYEE_NAME = "EmployeeName";
    public static final String KEY_EMPLOYEE_GUID = "EmployeeGuid";

    // PRODUCT TABLE
    public static final String KEY_PRODUCT_ID = "ProductId";
    public static final String KEY_PRODUCT_NAME = "ProductName";
    public static final String KEY_PRODUCT_CODE = "ProductCode";
    public static final String KEY_PRODUCT_IMAGE_URL = "ProductImageURL";
    public static final String KEY_PRODUCT_PRICE = "Price";

    // COLOR TABLE
    public static final String KEY_COLOR_ID = "ColorId";
    public static final String KEY_COLOR_NAME = "ColorName";
    public static final String KEY_COLOR_HEX_CODE = "HexCode";

    // SIZES TABLE
    public static final String KEY_SIZE_ID = "SizeId";
    public static final String KEY_SIZE_NAME = "SizeName";
    public static final String KEY_SIZE_CODE = "Code";

    //REturnRequestModel
    public static final String KEY_RETURN_QUANTITY = "ReturnQuantity";
    public static final String KEY_REFUND_AMOUNT = "RefundAmount";
    public static final String KEY_RETURN_REQUEST_PRODUCT_MODEL = "ReturnRequestProducts";

    // PCSPQ TABLE
    public static final String KEY_PCSPQ_EVENT_ID = Constants.KEY_EVENT_ID;
    public static final String KEY_PCSPQ_VENUES_ID = Constants.KEY_VENUES_ID;
    public static final String KEY_PCSPQ_PRODUCT_ID = Constants.KEY_PRODUCT_ID;
    public static final String KEY_PCSPQ_COLOR_ID = Constants.KEY_COLOR_ID;
    public static final String KEY_PCSPQ_SIZE_ID = Constants.KEY_SIZE_ID;
    public static final String KEY_PCSPQ_PRICE = Constants.KEY_PRODUCT_PRICE;
    public static final String KEY_PCSPQ_ALLOCATED_QUT = "AllocatedQty";
    public static final String KEY_PCSPQ_ALLOCATED_QUT_SYNC = "AllocatedQtySync";

    // EVENT DESIGN
    public static final String KEY_DESIGN_ID = "DesignId";
    public static final String KEY_DESIGN_NAME = "DesignName";
    public static final String KEY_DESIGN_NO = "DesignNo";
    public static final String KEY_DESIGN_PRICE = "DesignPrice";
    public static final String KEY_DESIGN_TRANSFER_PRICE = "DesignTransferPrice";
    public static final String KEY_DESIGN_IMAGE_URL = "DesignImageURL";

    //Country TABLE
    public static final String KEY_COUNTRY_id = "CountryId";
    public static final String KEY_COUNTRY_NAME = "CountryName";
    public static final String KEY_SELECTE_COUNTRY = "Select Country";


    // STATE TABLE
    public static final String KEY_StateProvince_Id = "StateProvinceId";
    public static final String KEY_STATE_NAME = "StateName";
    public static final String KEY_SELECTE_STATE = "Select State";

    public static final String KEY_STRING_DATA = "CityData";
    public static final String KEY_SATETEMANGE_DATA = "CityStatus";
    public static final String KEY_SELECTE_CITY = "Select City";

    // CITY TABLE
    /*public static final String KEY_CITY_id = "CityId";
    public static final String KEY_CITY_NAME = "CityName";*/

    public static final String KEY_CITY_id = "Id";
    public static final String KEY_CITY_NAME = "Name";

    //Configuration TABLE
    public static final String KEY_Configuration_Id = "ConfigurationId";
    public static final String KEY_Configuration_Key = "ConfigKey";
    public static final String KEY_Configuration_Value = "ConfigValue";


    //Contact INFo
    public static final String KEY_FIRST_NAME = "Customer_FirstName";
    public static final String KEY_LAST_NAME = "Customer_Lastname";
    public static final String KEY_POSTION = "Customer_Position";
    public static final String KEY_EMAILID = "Customer_Email";
    public static final String KEY_ADDRESS_ONE = "Customer_Address1";
    public static final String KEY_ADDRESS_TWO = "Customer_Address2";
    public static final String KEY_SPINNER_CONTACT_COUNTRY = "Customer_CountryId";
    public static final String KEY_SPINNER_CONTACT_STATE = "Customer_StateId";
    public static final String KEY_SPINNER_CONTACT_CITY = "Customer_CityName";
    public static final String KEY_ZIPCODE = "Customer_ZipCode";
    public static final String KEY_PHONE_NUMBER = "Customer_PhoneNo";
    public static final String BEST_TIME_TO_CALL = "Customer_BestTimeToCall";


    // Event InFo
    public static final String KEY_EVENT_NAME_EVNETINFO = "Event_Name";
    public static final String KEY_NO_OF_PARTICIPANTS = "Event_NoOfParticipant";
    public static final String KEY_DECISION_MAKER = "IsDecisionMaker";
    public static final String KEY_DATE_EVENTINFO = "Event_Date";
    public static final String KEY_CUSTOM_PRODUCT = "CustomProduct";
    public static final String KEY_YOUR_WISHlIST = "Wishlist";
    public static final String KEY_SPINNER_EVENT_COUNTRY = "Event_CountryId";
    public static final String KEY_SPINNER_EVENT_STATE = "Event_StateId";
    public static final String KEY_SPINNER_EVENT_CITY = "Event_CityName";

    // BUSINESS CARD

    public static final String KEY_LEAD_CAPTURED_BY = "EmployeeId";
    public static final String KEY_LEAD_CAPTURED_BY_Name = "EmployeeBName";
    public static final String KEY_LEADS_EmployeeGuid = "EmployeeGuid";
    public static final String KEY_Leads_EmployeeName = "EmployeeName";
    public static final String KEY_CARS_GIVEN_TO_CLIENT = "IsCardGivenToClient";
    public static final String KEY_FRONT_IMAGE = "BusinessCardFront";
    public static final String KEY_BACK_IMAGE = "BusinessCardBack";
    public static final String KAY_STATUS_Leads = "LeadsStatus";

    //Design Location
    public static final String KEY_Location_ID = "LocationId";
    public static final String KEY_LocationName = "LocationName";

    // IMAGE TYPE KEY
    public static final String KEY_IMAGE_CAPTURE_TYPE_FIRST = "First";
    public static final String KEY_IMAGE_CAPTURE_TYPE_SECOND = "Second";
    public static final String TAG_CHOOSE_A_FILE = "Select file";
    public static final String TAG_CHOOSE_FROM_GALLERY = "Choose from Gallery";
    public static final String TAG_CHOOSE_TAKE_PHONE = "Take Photo";
    public static final String TAG_CHOOSE_TAKE_CANCEL = "Cancel";
    public static final String TAG_TITLE_ADD_PHONE = "Add Phone";

    //LEADS INFO
    public static final String KEY_LEAD_GUID = "LeadGuid";
    public static final String KEY_LEAD_LEADID = "LeadId";
    public static final String KEY_LEAD_SERVERID = "ServerID";
    public static final String KEY_EVENT_ID_LEADS = "EventId";
    public static final String KEY_VENUE_ID_LEADS = "VenueId";
    public static final String KEY_LEADS_BY_GUID = "LeadGuid";

    //Donation
    public static final String KEY_GIFTS_PROMO_GUID = "GiftsPromoGuid";
    public static final String KEY_GIFTS_PROMO_ID = "GiftsPromoId";
    public static final String KEY_DONATION_EVENT_ID = "EventId";
    public static final String KEY_DONATION_VENUE_ID = "VenueId";
    public static final String KEY_DONATION_Receiver_FirstName = "Receiver_FirstName";
    public static final String KEY_DONATION_Receiver_Lastname = "Receiver_Lastname";
    public static final String KEY_DONATION_Receiver_Position = "Receiver_Position";
    public static final String KEY_DONATION_Receiver_Email = "Receiver_Email";
    public static final String KEY_DONATION_Receiver_PhoneNo = "Receiver_PhoneNo";
    public static final String KEY_DONATION_Receiver_BestTimeToCall = "Receiver_BestTimeToCall";
    public static final String KEY_DONATION_Receiver_Address1 = "Receiver_Address1";
    public static final String KEY_DONATION_Receiver_Address2 = "Receiver_Address2";
    public static final String KEY_DONATION_Receiver_CountryId = "Receiver_CountryId";
    public static final String KEY_DONATION_Receiver_StateId = "Receiver_StateId";
    public static final String KEY_DONATION_Receiver_CityName = "Receiver_CityName";
    public static final String KEY_DONATION_Receiver_ZipCode = "Receiver_ZipCode";
    public static final String KEY_DONATION_VoucherNumber = "VoucherNumber";
    public static final String KEY_DONATION_DonationTypeId = "DonationTypeId";
    public static final String KEY_DONATION_DonationTypeName = "DonationTypeName";
    public static final String KEY_DONATION_Amount = "Amount";
    public static final String KEY_DONATION_Date = "Date";
    public static final String KEY_DONATION_GiverName = "GiverName";
    public static final String KEY_DONATION_GiverPhoneNumber = "GiverPhoneNumber";
    public static final String KEY_DONATION_EmployeeId = "EmployeeId";
    public static final String KEY_DONATION_EmployeeName = "EmployeeName";
    public static final String KEY_Donation_employeeGUID = "EmployeeGuid";
    public static final String KEY_DONATION_OrderGuid = "OrderGuid";
    public static final String KAY_STATUS_ORDER = "DonationStatus";
    public static final String KEY_DELETE_SYNC = "IsDeleted";
    public static final String KEY_DELETE_SYNC_UPdate = "Update";
    public static final String KEY_UPDATE_Again_UPdate = "AgainUpdate";
    public static final String KEY_DONATION_CAREATE_DATE = "CreatedDate";
    public static final String KEY_Leads_CAREATE_DATE = "CreatedDate";

    // Setting CONFIGURATION
    public static final String KEY_CONFIG_TYPE = "DonationType";

    //Setting Currencies Name
    public static final String KEY_SETTING_CURRENCIES_NAME = "currencysettings.primarystorecurrencyid";
    public static final String KEY_DISCOUNT_PASSWORD = "storeinformationsettings.discountpassword";


    //Currencies
    public static final String KEY_Currency_Id = "CurrencyId";
    public static final String KEY_CURRENCY_NAME = "CurrencyName";
    public static final String KEY_CURRENCY_CODE = "CurrencyCode";
    public static final String KEY_CURRENCY_SYMBOL = "CurrencySymbol";
    public static final String KEY_CURRENCY_RATE = "CurrencyRate";

    //Settings
    public static final String KEY_Setting_Id = "SettingId";
    public static final String KEY_Setting_Name = "SettingName";
    public static final String KEY_Setting_Value = "SettingValue";


    // TaxRate
    public static final String KEY_EventId_TAX = "EventId";
    public static final String KEY_TaxRateId_TAX = "TaxRateId";
    public static final String KEY_GSTPercentage_TAX = "GSTPercentage";
    public static final String KEY_PSTPercentage_TAX = "PSTPercentage";
    public static final String KEY_Percentage_TAX = "Percentage";

    public static final String KEY_TRANSFER_GUID = "TransferGuid";
    //Place Order Sales
    public static final String KEY_ORDER_GUID = "OrderGuid";
    public static final String KEY_EVENT_ID_SALES = "EventId";
    public static final String KEY_VENUE_ID = "VenueId";
    public static final String key_employee_id = "EmployeeId";
    public static final String KEY_isPickupLater = "isPickupLater";
    public static final String KEY_EMPLOYEE_NAME_SALES = "EmployeeName";
    public static final String KEY_POSPaymentMethodName = "POSPaymentMethodName";
    public static final String KEY_PaymentMethodSystemName = "PaymentMethodSystemName";
    public static final String KEY_CustomerCurrencyCode = "CustomerCurrencyCode";
    public static final String KEY_CurrencyRate = "CurrencyRate";
    public static final String KEY_OrderSubtotalInclTax = "OrderSubtotalInclTax";
    public static final String KEY_OrderSubtotalExclTax = "OrderSubtotalExclTax";
    public static final String KEY_OrderSubTotalDiscountInclTax = "OrderSubTotalDiscountInclTax";
    public static final String KEY_OrderSubTotalDiscountExclTax = "OrderSubTotalDiscountExclTax";
    public static final String KEY_TaxRates = "TaxRates";
    public static final String KEY_OrderTax = "OrderTax";
    public static final String KEY_OrderDiscount = "OrderDiscount";
    public static final String KEY_OrderTotal = "OrderTotal";
    public static final String KEY_CustomerEmail = "CustomerEmail";
    public static final String KEY_PickUpVenue = "PickUpVenue";
    public static final String KEY_Payment_Card = "Payment_Card";
    public static final String KEY_CardType = "CardType";
    public static final String KEY_CardName = "CardName";
    public static final String KEY_CardNumber = "CardNumber";
    public static final String KEY_CardCvv2 = "CardCvv2";
    public static final String KEY_CardExpirationMonth = "CardExpirationMonth";
    public static final String KEY_CardExpirationYear = "CardExpirationYear";
    public static final String KEY_AuthorizationTransactionId = "AuthorizationTransactionId";
    public static final String KEY_Payment_Cheque = "Payment_Cheque";
    public static final String KEY_NameOfBank = "NameOfBank";
    public static final String KEY_ChequeNo = "ChequeNo";
    public static final String KEY_ChequeDate = "ChequeDate";
    public static final String KEY_ChequeAmount = "ChequeAmount";
    public static final String KEY_Payment_Cash = "Payment_Cash";
    public static final String KEY_CustomerLastName = "CustomerLastName";
    public static final String KEY_PhoneLastFourDigits = "PhoneLastFourDigits";
    public static final String KEY_CashReceived = "CashReceived";
    public static final String KEY_ChangeGiven = "ChangeGiven";
    public static final String KEY_OrderItems = "OrderItems";
    public static final String KEY_OrderGuid = "OrderGuid";
    public static final String KEY_OrderItemGuid = "OrderItemGuid";
    public static final String KEY_ProductId = "ProductId";
    public static final String KEY_Quantity = "Quantity";
    public static final String KEY_PriceInclTax = "PriceInclTax";
    public static final String KEY_PriceExclTax = "PriceExclTax";
    public static final String KEY_DiscountAmountInclTax = "DiscountAmountInclTax";
    public static final String KEY_DiscountAmountExclTax = "DiscountAmountExclTax";
    public static final String KEY_ColorId = "ColorId";
    public static final String KEY_OrderItemSizeQuantities = "OrderItemSizeQuantities";
    public static final String KEY_OrderItemGuid_SALES = "OrderItemGuid";
    public static final String KEY_OrderItemSizeQtyGuid = "OrderItemSizeQtyGuid";
    public static final String KEY_SizeId = "SizeId";
    public static final String KEY_QtyTranferred = "QtyTranferred";
    public static final String KEY_Quantity_SALES = "Quantity";
    public static final String KEY_UnitPrice = "UnitPrice";
    public static final String KEY_OrderItemDesigns = "OrderItemDesigns";
    public static final String KEY_OrderItemSizeQtyGuid_SALES = "OrderItemSizeQtyGuid";
    public static final String KEY_SizeId_sales = "SizeId";
    public static final String KEY_Quantity_SIZE_SALES = "Quantity";
    public static final String KEY_UnitPrice_SIZE = "UnitPrice";
    public static final String KEY_OrderItemDesigns_SALES = "OrderItemDesigns";
    public static final String KEY_OrderItemGuid_DESIGN_SALES = "OrderItemGuid";
    public static final String KEY_OrderItemDesignGuid = "OrderItemDesignGuid";
    public static final String KEY_DesignId = "DesignId";
    public static final String KEY_DecorationPrice = "DecorationPrice";
    public final static String KEY_Common_Id = "common_id";
    public final static String KEY_Total_Price = "total";
    public final static String KEY_PST_PERCENTAGE = "PSTPercentage";
    public final static String KEY_PST_Price = "PSTAmount";
    public final static String KEY_GST_PERCENTAGE = "GSTPercentage";
    public final static String KEY_GST_PRICE = "GSTAmount";
    public final static String KEY_DESIGNBYLOCATION = "DesignLocationByDesignIdModel";

    public final static String warring_percentage = "Please Enter Percentage";
    public final static String warring_amount = "Please Enter Amount";

    public final static String ERROR_PSW = "Please enter valid discount password.";
    public final static String Error_CASH_PAYMENT = "Amount Payable is greater then received cash.";
    public final static String Error_VENUE = "Please select or enter all field on page.";
    public final static String Error_VENUE_GRETERTHEN = "Transferred quantity is greater then quantity on hand.";
    public final static String SUCCESSFULLY_LEADS = "Lead has been added successfully.";
    public final static String SUCCESSFULLY_UPDATE_LEADS = "Lead has been updated successfully.";
    public final static String SUCCESSFULLY_GIFT_PROMO = "Gift & Promo has been added successfully.";
    public final static String SUCCESSFULLY_GIFT_PROMO_UPADTE = "Gift & Promo has been updated successfully.";
    public final static String SUCCESSFULLY_PAYMENT = "Order placed successfully.";
    public final static String Error_Cheque_PAYMENT = "Amount Payable is greater then received amount.";
    // Final Place Order Data
    public final static String Key_OrderGuid_OrderId = "OrderId";
    public final static String Key_OrderCode = "OrderCode";
    public final static String Key_IsUpdate = "IsUpdate";
    public final static String Key_Order_deciveid = "DeviceId";
    public final static String Key_LastOrderNo = "LastOrderNo";
    public final static String Key_OrderGuid_finalOrder = "OrderGuid";
    public final static String Key_Product_ID = "ProductId";
    public final static String Key_EventId_finalOrder = "EventId";
    public final static String Key_VenueId_finalOrder = "VenueId";
    public final static String Key_EmployeeId_finalOrder = "EmployeeId";
    public final static String Key_EmployeeGuid_finalOrder = "EmployeeGuid";
    public final static String Key_EmployeeName_finalOrder = "EmployeeName";
    public final static String Key_POSPaymentMethodName_finalOrder = "POSPaymentMethodName";
    public final static String Key_PaymentMethodSystemName_finalOrder = "PaymentMethodSystemName";
    public final static String Key_CustomerCurrencyCode_finalOrder = "CustomerCurrencyCode";
    public final static String Key_CurrencyRate_finalOrder = "CurrencyRate";
    public final static String Key_OrderSubtotalInclTax_finalOrder = "OrderSubtotalInclTax";
    public final static String Key_OrderSubtotalExclTax_finalOrder = "OrderSubtotalExclTax";
    public final static String Key_OrderSubTotalDiscountInclTax_finalOrder = "OrderSubTotalDiscountInclTax";
    public final static String Key_OrderSubTotalDiscountExclTax_finalOrder = "OrderSubTotalDiscountExclTax";
    public final static String Key_TaxRates_finalOrder = "TaxRates";
    public final static String Key_OrderTax_finalOrder = "OrderTax";
    public final static String Key_OrderDiscount_finalOrder = "OrderDiscount";
    public final static String Key_OrderTotal_finalOrder = "OrderTotal";
    public final static String Key_CustomerEmail_finalOrder = "CustomerEmail";
    public final static String Key_PickUpVenue = "PickUpVenue";
    public final static String Key_PickUpDate = "PickUpDate";
    public final static String Key_PickedUpOn = "PickedUpOn";
    public final static String Key_CustomerPhoneNo = "CustomerPhoneNo";
    public final static String Key_OrderDate = "OrderDate";
    public final static String Key_Sync_order = "SyncOrder";
    public final static String Key_isTransfers = "isTransfers";
    public final static String Key_ReturnRequestId = "ReturnRequestId";
    //Payment_Card
    public final static String Key_Payment_Card_finalOrder = "Payment_Card";
    public final static String Key_CardType_finalOrder = "CardType";
    public final static String Key_CardName_finalOrder = "CardName";
    public final static String Key_CardNumber_finalOrder = "CardNumber";
    public final static String Key_CardCvv2_finalOrder = "CardCvv2";
    public final static String Key_CardExpirationMonth_finalOrder = "CardExpirationMonth";
    public final static String Key_CardExpirationYear_finalOrder = "CardExpirationYear";
    public final static String Key_AuthorizationTransactionId_finalOrder = "AuthorizationTransactionId";
    //Cheque
    public final static String Key_Payment_Cheque_finalOrder = "Payment_Cheque";
    public final static String Key_NameOfBank_finalOrder = "NameOfBank";
    public final static String Key_ChequeNo_finalOrder = "ChequeNo";
    public final static String Key_ChequeDate_finalOrder = "ChequeDate";
    public final static String Key_ChequeAmount_finalOrder = "ChequeAmount";
    //Payment_Cash
    public final static String Key_Payment_Cash_finalOrder = "Payment_Cash";
    public final static String Key_CustomerLastName_finalOrder = "CustomerLastName";
    public final static String Key_PhoneLastFourDigits_finalOrder = "PhoneLastFourDigits";
    public final static String Key_CashReceived_finalOrder = "CashReceived";
    public final static String Key_ChangeGiven_finalOrder = "ChangeGiven";
    //Payment_SplitPayment : //Credit_Card
    public final static String Key_Payment_SplitPayment_finalOrder = "Payment_SplitPayment";
    public final static String Key_PaymentMethodSystemName_SPLITE_finalOrder_Credit_Card = "PaymentMethodSystemName";
    public final static String Key_Amount_SPLITE_finalOrder_Credit_Card = "Amount";
    public final static String Key_CardType_SPLITE_finalOrder_Credit_Card = "CardType";
    public final static String Key_CardName_SPLITE_finalOrder_Credit_Card = "CardName";
    public final static String Key_CardNumber_SPLITE_finalOrder_Credit_Card = "CardNumber";
    public final static String Key_CardCvv2_SPLITE_finalOrder_Credit_Card = "CardCvv2";
    public final static String Key_CardExpirationMonth_SPLITE_finalOrder_Credit_Card = "CardExpirationMonth";
    public final static String Key_CardExpirationYear_SPLITE_finalOrder_Credit_Card = "CardExpirationYear";
    public final static String Key_AuthorizationTransactionId_SPLITE_finalOrder_Credit_Card = "AuthorizationTransactionId";
    public final static String Key_NameOfBank_SPLITE_finalOrder_Credit_Card = "NameOfBank";
    public final static String Key_ChequeNo_SPLITE_finalOrder_Credit_Card = "ChequeNo";
    public final static String Key_ChequeDate_SPLITE_finalOrder_Credit_Card = "ChequeDate";
    public final static String Key_ChequeAmount_SPLITE_finalOrder_Credit_Card = "ChequeAmount";
    public final static String Key_CustomerLastName_SPLITE_finalOrder_Credit_Card = "CustomerLastName";
    public final static String Key_PhoneLastFourDigits_SPLITE_finalOrder_Credit_Card = "PhoneLastFourDigits";
    public final static String Key_CashReceived_SPLITE_finalOrder_Credit_Card = "CashReceived";
    public final static String Key_ChangeGiven_SPLITE_finalOrder_Credit_Card = "ChangeGiven";

    //Date Ane Time Place Order
    public final static String KEY_DATETIME = "DateTime";
    public final static String KEY_ORDER_DATE = "OrderDate";
    public final static String KEY_CREATE_ON = "CreatedOn";
    public final static String KEY_CREATE_DATE = "CreatedDate";
    public final static String KEY_UPDATE_ON = "UpdatedOn";

    //Error Message
    public final static String KEY_TRY_AGAIN = "Try again..";
    public final static String KEY_EMPLOYEE_NAME_existed = "Employee Name is already existed";

    public final static String KEY_TRANSFER_PRODUCT_CODE = "TO";
    public final static String KEY_TRANSFER_NO_COLOR = "No Color";

    public final static String CHANGE_BY = "Change by SHAILESH";

    public static final String FILE_NAME_CITY_JSON = "CityConfig.json";
    public static final String FILE_CITY_DATA = "config.json";
    public static final String KEY_PICKUP_LATER = "Pickup Later";
    public static final String KEY_DELIVERED = "Delivered";
    public static final String KEY_TransferDate = "TransferDate";
    public static final String KEY_IsReceiveStock = "IsReceiveStock";
    public static final String KEY_FromVenueId = "FromVenueId";
    public static final String KEY_ToVenueId = "ToVenueId";
    public static final String KEY_ReceivedTransferFromTo = "ReceivedTransferFromTo";
    public static final String KEY_ApperalTransferProducts = "ApperalTransferProducts";

    public static final String TAG_LIST = "List_Fragment";
    public static final String TAG_STATES = "Status_Fragment";
    public static final String TAG_TRANSFER = "Transfer_Fragment";

    public static final String TAG_PASSED_VENUE = "Venue";
    public static final String TAG_PASSED_MAIN_INVENTORY = "MainInventory";
    public static final String TAG_PASSED_PURCHASE_ORDER = "PurchaseOrder";

    /**
     * STATES TRANSFER ORDER
     */
    public static final String TAG_RECEIVED = "Received";
    public static final String TAG_TRANSFERRED = "Transferred";
}