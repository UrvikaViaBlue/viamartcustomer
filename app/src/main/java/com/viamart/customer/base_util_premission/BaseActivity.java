package com.viamart.customer.base_util_premission;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;
import com.leo.simplearcloader.SimpleArcLoader;

public  abstract class BaseActivity  extends AppCompatActivity {

    private SimpleArcDialog mDialog;
    private ProgressDialog progress;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
    }

    public boolean isOnline() {
        return ConnectionUtils.getInstance().isConnectingToInternet(this);
    }

    public void progressbarShow(){
        progress=new ProgressDialog(this);
        progress.setMessage("Please wait");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setProgress(0);
        progress.show();
    }

    public void prigressBarHide(){
        progress.dismiss();
    }

    public void showInternetDialog() {
        try {
            PromptDialog promptDialog = new PromptDialog(this);
            promptDialog.setDialogType(PromptDialog.DIALOG_TYPE_WARNING);
           // promptDialog.setAnimationEnable(true);
            promptDialog.setTitleText(Constants.TAG_NO_INTERNET_CONNECTION);
            promptDialog.setContentText(Constants.TAG_NO_INTERNET_CONNECTION_MESSAGE);
            promptDialog.setPositiveListener(Constants.TAG_OK, PromptDialog::dismiss);
            promptDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showProgressDialog() {
        try {
            if (mDialog == null) {
                ArcConfiguration configuration = new ArcConfiguration(this);
                configuration.setLoaderStyle(SimpleArcLoader.STYLE.SIMPLE_ARC);
                configuration.setText(Constants.TAG_PLEASE_WAIT);
                configuration.setColors(Constants.mColors);
                mDialog = new SimpleArcDialog(this);
                mDialog.setConfiguration(configuration);
                mDialog.setCanceledOnTouchOutside(false);
                mDialog.showWindow(true);
            }
            if (!mDialog.isShowing()) {
                mDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideProgressDialog() {
        try {
            if (mDialog != null && mDialog.isShowing()) {
                mDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
