package com.viamart.customer.model;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.viamart.customer.db.DataConverter;

import java.io.Serializable;
import java.util.List;

@Entity(tableName = "addToCartData")
public class AddtoCartData implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "quantity")
    @SerializedName("quantity")
    @Expose
    private int quantity;


    @SerializedName("data")
    @Expose
    @TypeConverters(DataConverter.class)
    private StoreCategory.Product storeProductData;


    @ColumnInfo(name = "variant_name")
    @SerializedName("variant_name")
    @Expose
    private String  variantName;


    @ColumnInfo(name = "variant_price")
    @SerializedName("variant_price")
    @Expose
    private double variantPrice;


    public String getVariantName() {
        return variantName;
    }

    public void setVariantName(String variantName) {
        this.variantName = variantName;
    }

    public double getVariantPrice() {
        return variantPrice;
    }

    public void setVariantPrice(double variantPrice) {
        this.variantPrice = variantPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public StoreCategory.Product getStoreProductData() {
        return storeProductData;
    }

    public void setStoreProductData(StoreCategory.Product storeProductData) {
        this.storeProductData = storeProductData;
    }


    public class Datum implements Serializable {

        @SerializedName("sp_id")
        @Expose
        private String spId;
        @SerializedName("product_id")
        @Expose
        private String productId;
        @SerializedName("product_name")
        @Expose
        private String productName;
        @SerializedName("product_image")
        @Expose
        private String productImage;
        @SerializedName("varients")
        @Expose
        private List<StoreProductDetails.Varient> varients = null;

        public String getSpId() {
            return spId;
        }

        public void setSpId(String spId) {
            this.spId = spId;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getProductImage() {
            return productImage;
        }

        public void setProductImage(String productImage) {
            this.productImage = productImage;
        }

        public List<StoreProductDetails.Varient> getVarients() {
            return varients;
        }

        public void setVarients(List<StoreProductDetails.Varient> varients) {
            this.varients = varients;
        }

    }

    public class Varient implements Serializable {

        @SerializedName("variant_name")
        @Expose
        private String variantName;
        @SerializedName("variant_price")
        @Expose
        private double variantPrice;
        @SerializedName("variant_qty")
        @Expose
        private String variantQty;
        @SerializedName("attribute_id")
        @Expose
        private String attributeId;

        public String getVariantName() {
            return variantName;
        }

        public void setVariantName(String variantName) {
            this.variantName = variantName;
        }

        public double getVariantPrice() {
            return variantPrice;
        }

        public void setVariantPrice(double variantPrice) {
            this.variantPrice = variantPrice;
        }

        public String getVariantQty() {
            return variantQty;
        }

        public void setVariantQty(String variantQty) {
            this.variantQty = variantQty;
        }

        public String getAttributeId() {
            return attributeId;
        }

        public void setAttributeId(String attributeId) {
            this.attributeId = attributeId;
        }
    }
}
