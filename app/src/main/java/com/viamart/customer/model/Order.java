package com.viamart.customer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Order {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Data> data = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Data> getOrder() {
        return data;
    }

    public void setOrder(List<Data> order) {
        this.data = order;
    }

    public class Data {

        @SerializedName("booking_id")
        @Expose
        private String bookingId;
        @SerializedName("address_id")
        @Expose
        private String addressId;
        @SerializedName("address1")
        @Expose
        private String address1;
        @SerializedName("address2")
        @Expose
        private String address2;
        @SerializedName("add_type")
        @Expose
        private String addType;
        @SerializedName("city_name")
        @Expose
        private String cityName;
        @SerializedName("store_id")
        @Expose
        private String storeId;
        @SerializedName("store_name")
        @Expose
        private String storeName;
        @SerializedName("store_address1")
        @Expose
        private String storeAddress1;
        @SerializedName("store_address2")
        @Expose
        private String storeAddress2;
        @SerializedName("store_city_id")
        @Expose
        private String storeCityId;
        @SerializedName("store_city_name")
        @Expose
        private String storeCityName;
        @SerializedName("booking_amount")
        @Expose
        private Double bookingAmount;
        @SerializedName("discount")
        @Expose
        private Double discount;
        @SerializedName("delivery_charge")
        @Expose
        private Double deliveryCharge;
        @SerializedName("total_amount")
        @Expose
        private Double totalAmount;
        @SerializedName("payment_method")
        @Expose
        private String paymentMethod;
        @SerializedName("payment_type")
        @Expose
        private String paymentType;
        @SerializedName("payment_status")
        @Expose
        private String paymentStatus;
        @SerializedName("tax")
        @Expose
        private Double tax;
        @SerializedName("booking_date")
        @Expose
        private String bookingDate;
        @SerializedName("order_time")
        @Expose
        private String orderTime;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("products")
        @Expose
        private List<Product> products = null;

        public String getBookingId() {
            return bookingId;
        }

        public void setBookingId(String bookingId) {
            this.bookingId = bookingId;
        }

        public String getAddressId() {
            return addressId;
        }

        public void setAddressId(String addressId) {
            this.addressId = addressId;
        }

        public String getAddress1() {
            return address1;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public String getAddress2() {
            return address2;
        }

        public void setAddress2(String address2) {
            this.address2 = address2;
        }

        public String getAddType() {
            return addType;
        }

        public void setAddType(String addType) {
            this.addType = addType;
        }

        public String getCityName() {
            return cityName;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getStoreAddress1() {
            return storeAddress1;
        }

        public void setStoreAddress1(String storeAddress1) {
            this.storeAddress1 = storeAddress1;
        }

        public String getStoreAddress2() {
            return storeAddress2;
        }

        public void setStoreAddress2(String storeAddress2) {
            this.storeAddress2 = storeAddress2;
        }

        public String getStoreCityId() {
            return storeCityId;
        }

        public void setStoreCityId(String storeCityId) {
            this.storeCityId = storeCityId;
        }

        public String getStoreCityName() {
            return storeCityName;
        }

        public void setStoreCityName(String storeCityName) {
            this.storeCityName = storeCityName;
        }

        public Double getBookingAmount() {
            return bookingAmount;
        }

        public void setBookingAmount(Double bookingAmount) {
            this.bookingAmount = bookingAmount;
        }

        public Double getDiscount() {
            return discount;
        }

        public void setDiscount(Double discount) {
            this.discount = discount;
        }

        public Double getDeliveryCharge() {
            return deliveryCharge;
        }

        public void setDeliveryCharge(Double deliveryCharge) {
            this.deliveryCharge = deliveryCharge;
        }

        public Double getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(Double totalAmount) {
            this.totalAmount = totalAmount;
        }

        public String getPaymentMethod() {
            return paymentMethod;
        }

        public void setPaymentMethod(String paymentMethod) {
            this.paymentMethod = paymentMethod;
        }

        public String getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }

        public String getPaymentStatus() {
            return paymentStatus;
        }

        public void setPaymentStatus(String paymentStatus) {
            this.paymentStatus = paymentStatus;
        }

        public Double getTax() {
            return tax;
        }

        public void setTax(Double tax) {
            this.tax = tax;
        }

        public String getBookingDate() {
            return bookingDate;
        }

        public void setBookingDate(String bookingDate) {
            this.bookingDate = bookingDate;
        }

        public String getOrderTime() {
            return orderTime;
        }

        public void setOrderTime(String orderTime) {
            this.orderTime = orderTime;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<Product> getProducts() {
            return products;
        }

        public void setProducts(List<Product> products) {
            this.products = products;
        }

    }

    public class Product {

        @SerializedName("product_id")
        @Expose
        private String productId;
        @SerializedName("product_name")
        @Expose
        private String productName;
        @SerializedName("product_image")
        @Expose
        private String productImage;
        @SerializedName("quantity")
        @Expose
        private String quantity;
        @SerializedName("price")
        @Expose
        private Double price;

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getProductImage() {
            return productImage;
        }

        public void setProductImage(String productImage) {
            this.productImage = productImage;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }

    }
}