package com.viamart.customer.model;

import java.io.Serializable;

public class TempPriceStore implements Serializable {

    double price;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
