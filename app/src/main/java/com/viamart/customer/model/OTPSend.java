package com.viamart.customer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OTPSend implements Serializable {

    @SerializedName("checkOtp")
    @Expose
    private CheckOtp checkOtp;

    public CheckOtp getCheckOtp() {
        return checkOtp;
    }

    public void setCheckOtp(CheckOtp checkOtp) {
        this.checkOtp = checkOtp;
    }

    public class Data implements Serializable {

        @SerializedName("customer_phone")
        @Expose
        private String customerPhone;
        @SerializedName("is_register")
        @Expose
        private Integer isRegister;
        @SerializedName("is_login")
        @Expose
        private String isLogin;
        @SerializedName("access_token")
        @Expose
        private String accessToken;

        public String getCustomerPhone() {
            return customerPhone;
        }

        public void setCustomerPhone(String customerPhone) {
            this.customerPhone = customerPhone;
        }

        public Integer getIsRegister() {
            return isRegister;
        }

        public void setIsRegister(Integer isRegister) {
            this.isRegister = isRegister;
        }

        public String getIsLogin() {
            return isLogin;
        }

        public void setIsLogin(String isLogin) {
            this.isLogin = isLogin;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

    }

    public class CheckOtp implements Serializable {

        @SerializedName("Error")
        @Expose
        private Integer error;
        @SerializedName("status")
        @Expose
        private Boolean status;
        @SerializedName("Message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private Data data;

        public Integer getError() {
            return error;
        }

        public void setError(Integer error) {
            this.error = error;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    }

} /*@SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }*/