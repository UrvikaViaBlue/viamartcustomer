package com.viamart.customer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Login implements Serializable {

    @SerializedName("userSignin")
    @Expose
    private UserSignin userSignin;

    public UserSignin getUserSignin() {
        return userSignin;
    }

    public void setUserSignin(UserSignin userSignin) {
        this.userSignin = userSignin;
    }

    public class UserSignin implements Serializable {

        @SerializedName("Error")
        @Expose
        private Integer error;
        @SerializedName("status")
        @Expose
        private Boolean status;
        @SerializedName("Message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private Data data;

        public Integer getError() {
            return error;
        }

        public void setError(Integer error) {
            this.error = error;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    }

    public class Data implements Serializable {

        @SerializedName("customer_phone")
        @Expose
        private String customerPhone;
        @SerializedName("is_register")
        @Expose
        private Integer isRegister;
        @SerializedName("login_token")
        @Expose
        private String loginToken;
        @SerializedName("optgenerate_date")
        @Expose
        private String optgenerateDate;
        @SerializedName("access_token")
        @Expose
        private String accessToken;

        public String getCustomerPhone() {
            return customerPhone;
        }

        public void setCustomerPhone(String customerPhone) {
            this.customerPhone = customerPhone;
        }

        public Integer getIsRegister() {
            return isRegister;
        }

        public void setIsRegister(Integer isRegister) {
            this.isRegister = isRegister;
        }

        public String getLoginToken() {
            return loginToken;
        }

        public void setLoginToken(String loginToken) {
            this.loginToken = loginToken;
        }

        public String getOptgenerateDate() {
            return optgenerateDate;
        }

        public void setOptgenerateDate(String optgenerateDate) {
            this.optgenerateDate = optgenerateDate;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

    }

}

   /* @SerializedName("userSignin")
    @Expose
    private UserSignin userSignin;

    public UserSignin getUserSignin() {
        return userSignin;
    }

    public void setUserSignin(UserSignin userSignin) {
        this.userSignin = userSignin;
    }

    public class Data implements Serializable {

        @SerializedName("customer_phone")
        @Expose
        private String customerPhone;
        @SerializedName("is_register")
        @Expose
        private Integer isRegister;
        @SerializedName("login_token")
        @Expose
        private String loginToken;
        @SerializedName("optgenerate_date")
        @Expose
        private String optgenerateDate;

        public String getCustomerPhone() {
            return customerPhone;
        }

        public void setCustomerPhone(String customerPhone) {
            this.customerPhone = customerPhone;
        }

        public Integer getIsRegister() {
            return isRegister;
        }

        public void setIsRegister(Integer isRegister) {
            this.isRegister = isRegister;
        }

        public String getLoginToken() {
            return loginToken;
        }

        public void setLoginToken(String loginToken) {
            this.loginToken = loginToken;
        }

        public String getOptgenerateDate() {
            return optgenerateDate;
        }

        public void setOptgenerateDate(String optgenerateDate) {
            this.optgenerateDate = optgenerateDate;
        }

    }

    public class UserSignin implements Serializable{

        @SerializedName("Error")
        @Expose
        private Integer error;
        @SerializedName("status")
        @Expose
        private Boolean status;
        @SerializedName("Message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private Data data;

        public Integer getError() {
            return error;
        }

        public void setError(Integer error) {
            this.error = error;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    }
*/
  /*  @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("fullname")
        @Expose
        private String fullname;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("phone_no")
        @Expose
        private String phoneNo;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("district")
        @Expose
        private String district;
        @SerializedName("city_id")
        @Expose
        private String cityId;
        @SerializedName("otp")
        @Expose
        private String otp;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("password")
        @Expose
        private String password;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getCityId() {
            return cityId;
        }

        public void setCityId(String cityId) {
            this.cityId = cityId;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

    }*/