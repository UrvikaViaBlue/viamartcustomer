package com.viamart.customer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class StoreCategory implements Serializable {
    @SerializedName("getStoreDetails")
    @Expose
    private GetStoreDetails getStoreDetails;

    public GetStoreDetails getGetStoreDetails() {
        return getStoreDetails;
    }

    public void setGetStoreDetails(GetStoreDetails getStoreDetails) {
        this.getStoreDetails = getStoreDetails;
    }

    public class Data implements Serializable {

        @SerializedName("product_category")
        @Expose
        private List<ProductCategory> productCategory = null;
        @SerializedName("products")
        @Expose
        private List<Product> products = null;

        public List<ProductCategory> getProductCategory() {
            return productCategory;
        }

        public void setProductCategory(List<ProductCategory> productCategory) {
            this.productCategory = productCategory;
        }

        public List<Product> getProducts() {
            return products;
        }

        public void setProducts(List<Product> products) {
            this.products = products;
        }

    }

    public class GetStoreDetails implements Serializable{

        @SerializedName("Error")
        @Expose
        private Integer error;
        @SerializedName("status")
        @Expose
        private Boolean status;
        @SerializedName("Message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private Data data;

        public Integer getError() {
            return error;
        }

        public void setError(Integer error) {
            this.error = error;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    }

    public class Product implements Serializable{

        @SerializedName("sp_id")
        @Expose
        private String spId;
        @SerializedName("product_name")
        @Expose
        private String productName;
        @SerializedName("store_id")
        @Expose
        private String storeId;
        @SerializedName("product_id")
        @Expose
        private String productId;
        @SerializedName("product_price")
        @Expose
        private double productPrice;
        @SerializedName("variant_name")
        @Expose
        private List<VariantName> variantName = null;
        @SerializedName("product_discription")
        @Expose
        private String productDiscription;
        @SerializedName("product_image")
        @Expose
        private String productImage;
        @SerializedName("category_id")
        @Expose
        private String categoryId;

        public String getSpId() {
            return spId;
        }

        public void setSpId(String spId) {
            this.spId = spId;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public double getProductPrice() {
            return productPrice;
        }

        public void setProductPrice(double productPrice) {
            this.productPrice = productPrice;
        }

        public List<VariantName> getVariantName() {
            return variantName;
        }

        public void setVariantName(List<VariantName> variantName) {
            this.variantName = variantName;
        }

        public String getProductDiscription() {
            return productDiscription;
        }

        public void setProductDiscription(String productDiscription) {
            this.productDiscription = productDiscription;
        }

        public String getProductImage() {
            return productImage;
        }

        public void setProductImage(String productImage) {
            this.productImage = productImage;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

    }

    public class ProductCategory implements Serializable {

        @SerializedName("category_id")
        @Expose
        private int categoryId;
        @SerializedName("category_name")
        @Expose
        private String categoryName;

        public int getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(int categoryId) {
            this.categoryId = categoryId;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

    }

    public class VariantName implements Serializable {

        @SerializedName("variant_name")
        @Expose
        private String variantName;
        @SerializedName("product_price")
        @Expose
        private double productPrice;

        public String getVariantName() {
            return variantName;
        }

        public void setVariantName(String variantName) {
            this.variantName = variantName;
        }

        public double getProductPrice() {
            return productPrice;
        }

        public void setProductPrice(double productPrice) {
            this.productPrice = productPrice;
        }

    }
}
   /* @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("store_id")
    @Expose
    private String storeId;
    @SerializedName("store_name")
    @Expose
    private String storeName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("address1")
    @Expose
    private String address1;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum implements Serializable{

        @SerializedName("category_id")
        @Expose
        private int categoryId;
        @SerializedName("category_name")
        @Expose
        private String categoryName;

        public int getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(int categoryId) {
            this.categoryId = categoryId;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

    }

}
*/