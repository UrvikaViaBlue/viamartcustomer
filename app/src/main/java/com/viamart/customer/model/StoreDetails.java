package com.viamart.customer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class StoreDetails implements Serializable {
    @SerializedName("getStores")
    @Expose
    private GetStores getStores;

    public GetStores getGetStores() {
        return getStores;
    }

    public void setGetStores(GetStores getStores) {
        this.getStores = getStores;
    }

    public class StoreDetail implements Serializable {

        @SerializedName("store_id")
        @Expose
        private String storeId;
        @SerializedName("store_name")
        @Expose
        private String storeName;
        @SerializedName("store_image")
        @Expose
        private String storeImage;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("start_time")
        @Expose
        private String startTime;
        @SerializedName("close_time")
        @Expose
        private String closeTime;
        @SerializedName("address1")
        @Expose
        private String address1;
        @SerializedName("address2")
        @Expose
        private String address2;
        @SerializedName("pincode")
        @Expose
        private String pincode;
        @SerializedName("lati")
        @Expose
        private String lati;
        @SerializedName("longi")
        @Expose
        private String longi;
        @SerializedName("city_id")
        @Expose
        private String cityId;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("verify_status")
        @Expose
        private String verifyStatus;
        @SerializedName("store_category_id")
        @Expose
        private String storeCategoryId;
        @SerializedName("category_id")
        @Expose
        private String categoryId;
        @SerializedName("category_name")
        @Expose
        private String categoryName;
        @SerializedName("category_image")
        @Expose
        private String categoryImage;
        @SerializedName("distance")
        @Expose
        private String distance;
        @SerializedName("city_name")
        @Expose
        private String cityName;
        @SerializedName("duration")
        @Expose
        private String duration;

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getStoreImage() {
            return storeImage;
        }

        public void setStoreImage(String storeImage) {
            this.storeImage = storeImage;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getCloseTime() {
            return closeTime;
        }

        public void setCloseTime(String closeTime) {
            this.closeTime = closeTime;
        }

        public String getAddress1() {
            return address1;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public String getAddress2() {
            return address2;
        }

        public void setAddress2(String address2) {
            this.address2 = address2;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getLati() {
            return lati;
        }

        public void setLati(String lati) {
            this.lati = lati;
        }

        public String getLongi() {
            return longi;
        }

        public void setLongi(String longi) {
            this.longi = longi;
        }

        public String getCityId() {
            return cityId;
        }

        public void setCityId(String cityId) {
            this.cityId = cityId;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getVerifyStatus() {
            return verifyStatus;
        }

        public void setVerifyStatus(String verifyStatus) {
            this.verifyStatus = verifyStatus;
        }

        public String getStoreCategoryId() {
            return storeCategoryId;
        }

        public void setStoreCategoryId(String storeCategoryId) {
            this.storeCategoryId = storeCategoryId;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public String getCategoryImage() {
            return categoryImage;
        }

        public void setCategoryImage(String categoryImage) {
            this.categoryImage = categoryImage;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }


        public String getCityName() {
            return cityName;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }
    }

    public class GetStores implements Serializable {

        @SerializedName("Error")
        @Expose
        private Integer error;
        @SerializedName("status")
        @Expose
        private Boolean status;
        @SerializedName("Message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private Data data;

        public Integer getError() {
            return error;
        }

        public void setError(Integer error) {
            this.error = error;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    }

    public class Data implements Serializable {

        @SerializedName("category_data")
        @Expose
        private List<CategoryDatum> categoryData = null;
        @SerializedName("store_details")
        @Expose
        private List<StoreDetail> storeDetails = null;

        public List<CategoryDatum> getCategoryData() {
            return categoryData;
        }

        public void setCategoryData(List<CategoryDatum> categoryData) {
            this.categoryData = categoryData;
        }

        public List<StoreDetail> getStoreDetails() {
            return storeDetails;
        }

        public void setStoreDetails(List<StoreDetail> storeDetails) {
            this.storeDetails = storeDetails;
        }

    }

    public class CategoryDatum implements Serializable {

        @SerializedName("category_id")
        @Expose
        private int categoryId;
        @SerializedName("category_name")
        @Expose
        private String categoryName;
        @SerializedName("category_image")
        @Expose
        private String categoryImage;
        @SerializedName("status")
        @Expose
        private String status;

        public int getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(int categoryId) {
            this.categoryId = categoryId;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public String getCategoryImage() {
            return categoryImage;
        }

        public void setCategoryImage(String categoryImage) {
            this.categoryImage = categoryImage;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

    }

}
   /* @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public class Datum implements Serializable{

        @SerializedName("store_id")
        @Expose
        private int storeId;
        @SerializedName("store_name")
        @Expose
        private String storeName;
        @SerializedName("store_image")
        @Expose
        private String storeImage;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("start_time")
        @Expose
        private String startTime;
        @SerializedName("close_time")
        @Expose
        private String closeTime;
        @SerializedName("address1")
        @Expose
        private String address1;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("lati")
        @Expose
        private String lati;
        @SerializedName("longi")
        @Expose
        private String longi;
        @SerializedName("store_category")
        @Expose
        private String storeCategory;
        @SerializedName("store_rating")
        @Expose
        private double storeRating;
        @SerializedName("store_distance")
        @Expose
        private Double distance;

        public int getStoreId() {
            return storeId;
        }

        public void setStoreId(int storeId) {
            this.storeId = storeId;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getStoreImage() {
            return storeImage;
        }

        public void setStoreImage(String storeImage) {
            this.storeImage = storeImage;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getCloseTime() {
            return closeTime;
        }

        public void setCloseTime(String closeTime) {
            this.closeTime = closeTime;
        }

        public String getAddress1() {
            return address1;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getLati() {
            return lati;
        }

        public void setLati(String lati) {
            this.lati = lati;
        }

        public String getLongi() {
            return longi;
        }

        public void setLongi(String longi) {
            this.longi = longi;
        }

        public String getStoreCategory() {
            return storeCategory;
        }

        public void setStoreCategory(String storeCategory) {
            this.storeCategory = storeCategory;
        }

        public double getStoreRating() {
            return storeRating;
        }

        public void setStoreRating(double storeRating) {
            this.storeRating = storeRating;
        }

        public Double getDistance() {
            return distance;
        }

        public void setDistance(Double distance) {
            this.distance = distance;
        }

    }
}*/
    /*@SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum implements Serializable {

        @SerializedName("store_id")
        @Expose
        private int storeId;
        @SerializedName("store_name")
        @Expose
        private String storeName;
        @SerializedName("store_image")
        @Expose
        private String storeImage;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("start_time")
        @Expose
        private String startTime;
        @SerializedName("close_time")
        @Expose
        private String closeTime;
        @SerializedName("address1")
        @Expose
        private String address1;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("lati")
        @Expose
        private String lati;
        @SerializedName("longi")
        @Expose
        private String longi;
        @SerializedName("store_category")
        @Expose
        private String storeCategory;
        @SerializedName("store_rating")
        @Expose
        private double  storeRating;

        public int getStoreId() {
            return storeId;
        }

        public void setStoreId(int storeId) {
            this.storeId = storeId;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getStoreImage() {
            return storeImage;
        }

        public void setStoreImage(String storeImage) {
            this.storeImage = storeImage;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getCloseTime() {
            return closeTime;
        }

        public void setCloseTime(String closeTime) {
            this.closeTime = closeTime;
        }

        public String getAddress1() {
            return address1;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getLati() {
            return lati;
        }

        public void setLati(String lati) {
            this.lati = lati;
        }

        public String getLongi() {
            return longi;
        }

        public void setLongi(String longi) {
            this.longi = longi;
        }

        public String getStoreCategory() {
            return storeCategory;
        }

        public void setStoreCategory(String storeCategory) {
            this.storeCategory = storeCategory;
        }

        public double  getStoreRating() {
            return storeRating;
        }

        public void setStoreRating(double  storeRating) {
            this.storeRating = storeRating;
        }

    }
}*/
