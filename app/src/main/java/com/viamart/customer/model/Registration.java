package com.viamart.customer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Registration implements Serializable {
    @SerializedName("userSignup")
    @Expose
    private UserSignup userSignup;

    public UserSignup getUserSignup() {
        return userSignup;
    }

    public void setUserSignup(UserSignup userSignup) {
        this.userSignup = userSignup;
    }
    public class Data implements Serializable {

        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("phone_no")
        @Expose
        private String phoneNo;
        @SerializedName("access_token")
        @Expose
        private String accessToken;
        @SerializedName("fullname")
        @Expose
        private Object fullname;
        @SerializedName("device_id")
        @Expose
        private String deviceId;
        @SerializedName("device_name")
        @Expose
        private Object deviceName;
        @SerializedName("device_type")
        @Expose
        private Object deviceType;
        @SerializedName("os_version")
        @Expose
        private Object osVersion;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("district")
        @Expose
        private Object district;
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("customer_image")
        @Expose
        private String customerImage;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public Object getFullname() {
            return fullname;
        }

        public void setFullname(Object fullname) {
            this.fullname = fullname;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public Object getDeviceName() {
            return deviceName;
        }

        public void setDeviceName(Object deviceName) {
            this.deviceName = deviceName;
        }

        public Object getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(Object deviceType) {
            this.deviceType = deviceType;
        }

        public Object getOsVersion() {
            return osVersion;
        }

        public void setOsVersion(Object osVersion) {
            this.osVersion = osVersion;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Object getDistrict() {
            return district;
        }

        public void setDistrict(Object district) {
            this.district = district;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getCustomerImage() {
            return customerImage;
        }

        public void setCustomerImage(String customerImage) {
            this.customerImage = customerImage;
        }

    }
    public class UserSignup {

        @SerializedName("Error")
        @Expose
        private Integer error;
        @SerializedName("status")
        @Expose
        private Boolean status;
        @SerializedName("Message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private Data data;

        public Integer getError() {
            return error;
        }

        public void setError(Integer error) {
            this.error = error;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    }
}
   /* @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
*/