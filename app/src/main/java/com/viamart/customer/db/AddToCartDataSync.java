package com.viamart.customer.db;

import android.content.Context;
import android.os.AsyncTask;

import com.viamart.customer.model.AddtoCartData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class AddToCartDataSync extends AsyncTask<Object, Object, List<AddtoCartData>> {

    private AsyncResponse mResponse;
    private Context mContext;

    public AddToCartDataSync(Context mContext, AsyncResponse asyncResponse) {
        this.mResponse = asyncResponse;
        this.mContext = mContext;
    }
    @Override
    protected List<AddtoCartData> doInBackground(Object... objects) {
        return null;
    }

    @Override
    protected void onPostExecute(List<AddtoCartData> result) {
        mResponse.onFinishProcess(result);
    }

    public void build() {
        this.execute();
    }

    private String readFile() throws IOException {
        BufferedReader reader = null;
        reader = new BufferedReader(new InputStreamReader(mContext.getAssets().open("data/event_json.json"), "UTF-8"));

        String content = "";
        String line;
        while ((line = reader.readLine()) != null) {
            content = content + line;
        }
        return content;

    }
}
