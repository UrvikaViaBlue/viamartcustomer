package com.viamart.customer.db;

import com.viamart.customer.model.AddtoCartData;

import java.util.List;

public interface AsyncResponse {

    void onStartProcess();

    void onFinishProcess(List<AddtoCartData> output);
}
