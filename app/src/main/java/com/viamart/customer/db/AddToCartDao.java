package com.viamart.customer.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.viamart.customer.model.AddtoCartData;

import java.util.List;

@Dao
public interface AddToCartDao {
    @Query("SELECT * FROM addToCartData")
    List<AddtoCartData> getAll();

    @Insert
    void insert(AddtoCartData task);

    @Delete
    void delete(AddtoCartData task);

    @Update
    void update(AddtoCartData task);
}
