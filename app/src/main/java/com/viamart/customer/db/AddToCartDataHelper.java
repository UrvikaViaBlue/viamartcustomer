package com.viamart.customer.db;

import android.content.Context;

import com.viamart.customer.model.AddtoCartData;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class AddToCartDataHelper {
    private static AddToCartDataHelper ourInstance;
    private AppDatabase mDatabase;

    public static AddToCartDataHelper getInstance(Context mContext) {
        if (ourInstance == null) {
            ourInstance = new AddToCartDataHelper(mContext);
        }
        return ourInstance;
    }

    private AddToCartDataHelper(Context mContext) {
        mDatabase = LocalCacheManager.getInstance(mContext).getAppDatabase();
    }

    public void onAddPlaceOrder(AddtoCartData addtoCartData) {
        Completable.fromAction(() -> {
            mDatabase.addtoCartData().insert(addtoCartData);
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onError(Throwable e) {

            }
        });
    }
    public void getAllCity(final AddToCartCallBack databaseCallback) {

        List<AddtoCartData> itemModels = new ArrayList<>();
        Completable.fromAction(() -> {
            itemModels.addAll(mDatabase.addtoCartData().getAll());
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onComplete() {
                if (databaseCallback != null) {
                    databaseCallback.onAddToCartLoaded(itemModels);
                }
            }

            @Override
            public void onError(Throwable e) {
                if (databaseCallback != null) {
                    databaseCallback.onDataNotAvailable();
                }
            }
        });
    }

    public interface AddToCartCallBack {

        void onAddToCartLoaded(List<AddtoCartData> users);

        void onDataNotAvailable();
    }
}
