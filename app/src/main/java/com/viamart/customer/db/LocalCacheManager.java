package com.viamart.customer.db;

import android.content.Context;

import androidx.room.Room;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.viamart.customer.base_util_premission.Constants;

public class LocalCacheManager {

    private static LocalCacheManager _instance;
    private AppDatabase mDatabase;

    public static LocalCacheManager getInstance(Context context) {
        if (_instance == null) {
            _instance = new LocalCacheManager(context);
        }
        return _instance;
    }

    /**
     * DATABASE CACHE MANAGER TO HELPER DATABASE ACCESS
     *
     * @param context
     */
    private LocalCacheManager(Context context) {
        mDatabase = Room.databaseBuilder(context, AppDatabase.class, Constants.DB_NAME)
                //.addMigrations(MIGRATION_1_2)
                .build();
    }

    private static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE users ADD COLUMN last_update INTEGER");
        }
    };

    public AppDatabase getAppDatabase() {
        return mDatabase;
    }

}
