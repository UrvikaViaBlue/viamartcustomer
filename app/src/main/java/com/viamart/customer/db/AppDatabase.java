package com.viamart.customer.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.viamart.customer.model.AddtoCartData;


@Database(entities = {AddtoCartData.class}, version = 1)
@TypeConverters(DataConverter.class)
public abstract  class AppDatabase  extends RoomDatabase {
    public abstract AddToCartDao addtoCartData();
}
