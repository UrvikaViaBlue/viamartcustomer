package com.viamart.customer.db;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.viamart.customer.model.StoreCategory;
import com.viamart.customer.model.StoreProductDetails;

import java.lang.reflect.Type;
import java.util.List;

public class DataConverter {

    @TypeConverter
    public String fromCountryLangList(StoreCategory.Product countryLang) {
        if (countryLang == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<StoreCategory.Product>() {}.getType();
        String json = gson.toJson(countryLang, type);
        return json;
    }

    @TypeConverter
    public StoreCategory.Product toCountryLangList(String countryLangString) {
        if (countryLangString == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<StoreCategory.Product>() {}.getType();
        StoreCategory.Product countryLangList = gson.fromJson(countryLangString, type);
        return countryLangList;
    }
}
