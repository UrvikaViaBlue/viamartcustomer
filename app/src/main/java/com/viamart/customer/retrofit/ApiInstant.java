package com.viamart.customer.retrofit;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.JsonObject;
import com.viamart.customer.model.Address;
import com.viamart.customer.model.Login;
import com.viamart.customer.model.OTPGet;
import com.viamart.customer.model.OTPSend;
import com.viamart.customer.model.Order;
import com.viamart.customer.model.PlaceOrderResponse;
import com.viamart.customer.model.Registration;
import com.viamart.customer.model.SetLatLong;
import com.viamart.customer.model.StoreCategory;
import com.viamart.customer.model.StoreCategoryFilter;
import com.viamart.customer.model.StoreDetails;
import com.viamart.customer.model.StoreProductDetails;
import com.viamart.customer.model.User;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;


public interface ApiInstant {

    public static final String GOOGLE_MAP_API_KEY_FOR_PLACE = "AIzaSyCcsMaxwqGHqOmeiCTVz3Z_Z4WcyyhP8Jk";

    public static String countrCode = "in";

    public static boolean checkInternetConnection(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }


    //@Headers("Content-Type:application/json")
    @POST("ApiController/loginByNumber")
        //@POST("api/getPhone")
    Call<Login> LoginUser(@Body JsonObject bean
    );


   // @Headers("Content-Type:application/json")
    @POST("ApiController/checkOtp")
    Call<OTPSend> OtpSendData(@Body JsonObject bean);




    Call<OTPGet> OtpData(@Body JsonObject object);


   // @Headers("Content-Type:application/json")
    @POST("ApiController/getStores")
    Call<StoreDetails> storeList(@Body JsonObject bean);

    @POST("ApiController/getStores")
    Call<StoreDetails> getStoreListCategoryWIse(@Body JsonObject bean);

    //
    @GET("ApiController/getStoreDetails")
    Call<StoreCategoryFilter> StoreCategoryFilter();




    //@Headers("Content-Type:application/json")
    @FormUrlEncoded
    @POST("ApiController/register")
    Call<Registration> Registartion(@Field("customer_phone") String phone_no,
                                    @Field("customer_email") String email,
                                    @Field("username") String fullname,
                                    @Field("device_uid") String device_id
    );

   // @Headers("Content-Type:application/json")
    @POST("ApiController/getStoreDetails")
    Call<StoreCategory> StoreProductDat(@Body JsonObject bean);


    @FormUrlEncoded
    @POST("api/addOrder")
    Call<PlaceOrderResponse> PlaceOrder(@Field("phone_no") String phone_no,
                                        @Field("device_id") String device_id,
                                        @Field("store_id") int store_id,
                                        @Field("category_id") String category_id,
                                        @Field("address_id") int address_id,
                                        @Field("payment_type") String payment_type,
                                        @Field("total_quantity") double total_quantity,
                                        @Field("discount") double discount,
                                        @Field("delivery_charge") double delivery_charge,
                                        @Field("promo_code") String promo_code,
                                        @Field("delivery_type") String delivery_type,
                                        @FieldMap Map<String, String> params
    );


   // @Headers("Content-Type:application/json")
    @POST("ApiController/getStoreDetails")
    Call<StoreCategory> getStoreCategories(@Body JsonObject object);

    @FormUrlEncoded
    @POST("api/getUser")
    Call<User> GetUserData(@Field("phone_no") String phone_no, @Field("device_id") String device_id);

    @FormUrlEncoded
    @POST("api/getUserAddresses")
    Call<Address> getUserAddresses(@Field("phone_no") String phone_no);

    @FormUrlEncoded
    @POST("api/addUserAddress")
    Call<Login> addUserAddress(@Field("phone_no") String phone_no,
                               @Field("address1") String address1,
                               @Field("city_name") String city_name,
                               @Field("longi") Double longi,
                               @Field("lati") Double lati,
                               @Field("add_type") String add_type
    );

    @FormUrlEncoded
    @POST("api/removeUserAddress")
    Call<Address> removeuseraddress(@Field("id") int id);

    @FormUrlEncoded
    @POST("getOrders")
    Call<Order> getOrders(@Field("phone_no") String phone_no);

    @FormUrlEncoded
    @POST("getOrders")
    Call<Order> getOrderDetails(@Field("phone_no") String phone_no, @Field("booking_id") String booking_id);
}