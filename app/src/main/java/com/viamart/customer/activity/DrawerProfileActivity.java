package com.viamart.customer.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.viamart.customer.R;
import com.viamart.customer.base_util_premission.BaseActivity;
import com.viamart.customer.fragment.DashBoardFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class DrawerProfileActivity extends BaseActivity implements View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener {

    DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle mDrawerToggle;
    NavigationView nvView;
    CircleImageView profile_image;
    FrameLayout frameLayout;
    @BindView(R.id.navigationView)
    BottomNavigationView navigationView;
    boolean connected = false;
    String newString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer_profile);
        ButterKnife.bind(this);
        profile_image = findViewById(R.id.profile_image);
        frameLayout = findViewById(R.id.frameLayout);
        nvView = findViewById(R.id.nvView);
        navigationView = (BottomNavigationView) findViewById(R.id.navigationView);
        navigationView.setOnNavigationItemSelectedListener(this);
        Bundle extras = getIntent().getExtras();

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            connected = true;
            loadFragment(new DashBoardFragment());
        } else {
            Toast.makeText(this, "No internet connection .", Toast.LENGTH_SHORT).show();
            connected = false;
        }
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onClick(View v) {
        Log.d("TAG", "onClick: ");
        if (v.getId() == R.id.profile_image) {

            mDrawerLayout.openDrawer(GravityCompat.END);
        }
    }


    public void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayout, fragment);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        DashBoardFragment firstFragment = (DashBoardFragment) getSupportFragmentManager().getFragments().get(0);
        firstFragment.onBackPressed();
        //  super.onBackPressed();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        Class fragmentClass = null;
        switch (item.getItemId()) {
            case R.id.dashboard:
                addFragment(new DashBoardFragment());
                break;
            case R.id.navigation_Order:
                Intent intent = new Intent(DrawerProfileActivity.this, OrderListActivity.class);
                startActivity(intent);
                break;
            case R.id.navigation_UserProfile:
                Intent intentProfile = new Intent(DrawerProfileActivity.this, ProfileActivity.class);
                startActivity(intentProfile);
                break;

            default:
                fragmentClass = DashBoardFragment.class;
        }
        item.setChecked(true);
        setTitle(item.getTitle());
        return false;
    }

    public void addFragment(Fragment f) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction = fragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.frameLayout, f);
        mFragmentTransaction.commit();
    }
}