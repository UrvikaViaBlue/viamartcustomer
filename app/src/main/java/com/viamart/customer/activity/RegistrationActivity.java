package com.viamart.customer.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import com.viamart.customer.R;
import com.viamart.customer.base_util_premission.BaseActivity;
import com.viamart.customer.model.Registration;
import com.viamart.customer.retrofit.ApiInstant;
import com.viamart.customer.retrofit.BaseRetrofit;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends BaseActivity implements View.OnClickListener {

   /* @BindView(R.id.checkTermCondition)
    CheckBox checkTermCondition;
    @BindView(R.id.termsCondition)
    AppCompatTextView termsCondition;*/
    @BindView(R.id.edtUserName)
    AppCompatEditText edtUserName;
    @BindView(R.id.edtEmail)
    AppCompatEditText edtEmail;
    @BindView(R.id.registration)
    AppCompatButton registration;
    SharedPreferences sh;
    String phoneNumber;
    private Context context = this;
    String android_device_id;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        registration.setOnClickListener(this);
        sh = getSharedPreferences("MySharedPref", Context.MODE_APPEND);
        phoneNumber = sh.getString("phoneNumber", "");
        android_device_id = sh.getString("Device_ID", "");
                /*Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);*/
    }

    @Override
    public void onClick(View v) {
        String userName = edtUserName.getText().toString();
        String email = edtEmail.getText().toString();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (userName.isEmpty()) {
            Toast.makeText(this, R.string.userNameError, Toast.LENGTH_SHORT).show();
        } else if (email.isEmpty()) {
            Toast.makeText(this, R.string.emailError, Toast.LENGTH_SHORT).show();
        } else if (!email.matches(emailPattern)) {
            Toast.makeText(this, R.string.validEmailError, Toast.LENGTH_SHORT).show();
        } else {
            Registartion(phoneNumber, email, userName);
        }
    }

    private void Registartion(String phone, String email, String userName) {
        progressbarShow();
        ApiInstant service = BaseRetrofit.getRetrofitInstance().create(ApiInstant.class);
        Call<Registration> call = service.Registartion(phone, email, userName, android_device_id);
        call.enqueue(new Callback<Registration>() {
            @Override
            public void onResponse(Call<Registration> call, Response<Registration> response) {
                prigressBarHide();
                if (response.isSuccessful()) {

                    if (response.body() != null) {

                        Registration registration = response.body();
                            String access_token = registration.getUserSignup().getData().getAccessToken();

                            SharedPreferences sharedPreferences
                                    = getSharedPreferences("MySharedPref",
                                    MODE_PRIVATE);
                            SharedPreferences.Editor myEdit
                                    = sharedPreferences.edit();
                            myEdit.putString("access_token", access_token);
                            myEdit.apply();
                            Intent intent = new Intent(RegistrationActivity.this, DrawerProfileActivity.class);
                            startActivity(intent);
                            finish();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Something went wrong...", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Registration> call, Throwable t) {
                prigressBarHide();
                Toast.makeText(getApplicationContext(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}