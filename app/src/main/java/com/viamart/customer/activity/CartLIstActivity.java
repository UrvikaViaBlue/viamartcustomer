package com.viamart.customer.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.viamart.customer.R;
import com.viamart.customer.adapter.CartListAdapter;
import com.viamart.customer.db.AddToCartDataHelper;
import com.viamart.customer.model.AddtoCartData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CartLIstActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.rvCartList)
    RecyclerView rvCartList;
    @BindView(R.id.imgBack)
    AppCompatImageView imgBack;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_list);
        ButterKnife.bind(this);
        imgBack.setOnClickListener(this);
        getCartList();
    }

    public void getCartList() {
        AddToCartDataHelper.getInstance(context).getAllCity(new AddToCartDataHelper.AddToCartCallBack() {
            @Override
            public void onAddToCartLoaded(List<AddtoCartData> users) {
                CartListAdapter cartListAdapter = new CartListAdapter(context, users);
                RecyclerView.LayoutManager mLayoutManagershopName = new LinearLayoutManager(context);
                rvCartList.setLayoutManager(mLayoutManagershopName);
                rvCartList.setItemAnimator(new DefaultItemAnimator());
                rvCartList.setAdapter(cartListAdapter);
            }

            @Override
            public void onDataNotAvailable() {

            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imgBack) {
            onBackPressed();
        }
    }
}
