package com.viamart.customer.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.viamart.customer.R;
import com.viamart.customer.adapter.AddressBookAdapter;
import com.viamart.customer.model.Address;
import com.viamart.customer.retrofit.ApiInstant;
import com.viamart.customer.retrofit.BaseRetrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressBookActivity extends AppCompatActivity {
    RecyclerView recyclerAddressList;
    String phoneno;

    private long mLastClickTime = 0;
    private Dialog dialogDeliveryAddress;
    private Dialog progressDialog;
    String city;
    private ShimmerFrameLayout mShimmerViewContainer;
    Boolean FromProfile = true;

    private LinearLayout linNoData;
    private TextInputEditText edtAddress1, edtAddress2, edtcity, edtAddType;
    private TextInputLayout txtWrapperAddress1, txtWrapperAddress2, txtWrappercity, txtWrapperAddType;
    private Button btnCancel, btnSaveAddress, btnGpsLocation, btn_addAddress;
    SharedPreferences sh;
    String phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_book);
        linNoData = (LinearLayout) findViewById(R.id.linNoData);
        //   mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        recyclerAddressList = findViewById(R.id.recycleraddressList);
        btn_addAddress = findViewById(R.id.btn_addAddress);
        FromProfile = getIntent().getBooleanExtra("FromProfile", true);
        sh = getSharedPreferences("MySharedPref", MODE_PRIVATE);
        phoneNumber = sh.getString("phoneNumber", "");

        getUserAddresses(phoneno);
        btn_addAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddressBookActivity.this, AddAddressActivity.class);
                startActivity(intent);


            }
        });
        ((ImageView) findViewById(R.id.imgBackOrderHIstory)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }

                mLastClickTime = SystemClock.elapsedRealtime();
                onBackPressed();
            }
        });

    }


    private void getUserAddresses(String phoneno) {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            /*Create handle for the RetrofitInstance interface*/
            try {
                ApiInstant service = BaseRetrofit.getRetrofitInstance().create(ApiInstant.class);
                Call<Address> call = service.getUserAddresses(phoneno);
                call.enqueue(new Callback<Address>() {
                    @Override
                    public void onResponse(Call<Address> call, Response<Address> response) {
                        if (response.isSuccessful()) {
                            Address store = response.body();
                            List<Address.Data> datumList = store.getData();
                            if (datumList != null) {
                                if ((datumList.size() > 0)) {
                                    linNoData.setVisibility(View.GONE);

                                    if (datumList != null) {
                                        AddressBookAdapter addressBookAdapter = new AddressBookAdapter(datumList, getApplicationContext(), new AddressBookAdapter.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(View view, int position) {
                                                if (FromProfile) {

                                               finish();   }else {

                                                }
                                            }
                                        });


                                        Log.d("datalist", "onResponse: " + datumList);
                                        RecyclerView.LayoutManager mLayoutManagershopName = new LinearLayoutManager(getApplicationContext());
                                        recyclerAddressList.setLayoutManager(mLayoutManagershopName);
                                        recyclerAddressList.setItemAnimator(new DefaultItemAnimator());
                                        recyclerAddressList.setAdapter(addressBookAdapter);
                                    }

                                } else {
                                    linNoData.setVisibility(View.VISIBLE);

                                }
                            }
                        } else {
                            linNoData.setVisibility(View.VISIBLE);

                            Toast.makeText(getApplicationContext(), "Something went wrong...Please try", Toast.LENGTH_SHORT).show();
                        }
                    }


                    @Override
                    public void onFailure(Call<Address> call, Throwable t) {
                        Log.d("TAG", "onFailure: " + t);
                        linNoData.setVisibility(View.VISIBLE);

                        Toast.makeText(getApplicationContext(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Somthing went to wrong.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    protected void onPause() {
        // mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }
}