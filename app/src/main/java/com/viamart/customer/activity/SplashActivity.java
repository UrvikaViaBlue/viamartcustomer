package com.viamart.customer.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;

import com.viamart.customer.R;

public class SplashActivity extends AppCompatActivity {
    SharedPreferences sh;
    String phoneNumber;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        String android_device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
       /* SharedPreferences.Editor myEdit
                = sh.edit();
        myEdit.putString("deviceID", android_device_id);

*/
        Log.d("Device ID", "onCreate: " + android_device_id);
        sh = getSharedPreferences("MySharedPref", Context.MODE_APPEND);
        phoneNumber = sh.getString("phoneNumber", "");
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // This method will be executed once the timer is over
                if (!phoneNumber.equals("") && phoneNumber.length() > 0) {
                    Intent intent = new Intent(SplashActivity.this, DrawerProfileActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, 3000);
    }
}
