package com.viamart.customer.activity;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.textfield.TextInputLayout;
import com.viamart.customer.R;
import com.viamart.customer.adapter.RecyclerViewAdapter;
import com.viamart.customer.interfaceData.RecycleItemClickListner;
import com.viamart.customer.model.Login;
import com.viamart.customer.retrofit.ApiInstant;
import com.viamart.customer.retrofit.BaseRetrofit;
import com.viamart.customer.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAddressActivity extends FragmentActivity implements
        LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback {


    private long mLastClickTime = 0;

    private GoogleMap mMap;
    AppCompatEditText edtAddress1, edtAddress2, edtAddType;
    double lat;
    double lan;
    Double latti, longi;
    String location_type = "Home";
    public static RecycleItemClickListner recycleItemClickListner;
    Location mLastLocation = new Location(LocationManager.GPS_PROVIDER);
    private TextInputLayout txtWrapperAddress1;
    private Handler handler;
    Boolean isDrop = false;
    double latitude, longitude;
    LatLng originLatLong, destinationLatLong;

    String MainName, drop_address, drop_city, edtcity;
    String place;
    String placeID;
    ImageView imgback;
    LocationManager locationManager;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    TextView txt_change;
    private BottomSheetBehavior mBottomSheetBehavior1;
    RecyclerViewAdapter recyclerViewAdapter;
    LinearLayout add_address_bottom_sheet, card_search;

    List<Integer> img = new ArrayList<>();
    List<String> address = new ArrayList<>();
    RecyclerView.LayoutManager layoutManager;
    RecyclerView rv_addresses;
    AppCompatButton btnSaveAddressDialog;
    List<String> place_list = new ArrayList<>();

    String phoneno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
       // AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView_search);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        txt_change = findViewById(R.id.txt_change);
        btnSaveAddressDialog = findViewById(R.id.btnSaveAddressDialog);
        edtAddress1 = findViewById(R.id.edtAddress1);
        edtAddress2 = findViewById(R.id.edtAddress2);
        edtAddType = findViewById(R.id.edtAddType);
        // edtcity = findViewById(R.id.edtcity);
      //  card_search = findViewById(R.id.card_search);
        imgback = findViewById(R.id.imgBack);
        // Bundle extras = getIntent().getExtras();
        // if (extras.getBoolean("Bool")) {
        // latti = extras.getDouble("PreLat");
        //   longi = extras.getDouble("Prelong");
       /* if (getIntent().getBooleanExtra("check", true)) {
            mLastLocation.setLatitude(getIntent().getDoubleExtra("PreLat", 0d));
            mLastLocation.setLongitude(getIntent().getDoubleExtra("Prelong", 0d));
            LoadMap(mLastLocation);
        }*/
        //   }
        // longi = getIntent().getExtras().getDouble("Prelong");
        Log.d("TAG", "onMenuItemClick: " + latti + "     " + longi);

        add_address_bottom_sheet = findViewById(R.id.add_address_bottom_sheet);
        phoneno = "9723328289";
        btnSaveAddressDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation()) {
                    addUserAddress(phoneno);
                }
            }
        });
        ((ImageView) findViewById(R.id.imgBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }

                mLastClickTime = SystemClock.elapsedRealtime();
                onBackPressed();
            }
        });

        txtWrapperAddress1 = (TextInputLayout) findViewById(R.id.txtWrapperAddress1);

        edtAddress1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                txtWrapperAddress1.setErrorEnabled(false);
            }
        });
        txt_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*add_address_bottom_sheet.setVisibility(View.INVISIBLE);
              //  card_search.setVisibility(View.VISIBLE);*/
                Intent intent = new Intent(AddAddressActivity.this, SelectPlaceActivity.class);
                intent.putExtra("title", "Select DROP Location");
                intent.putExtra("latlan", "" + latitude + "," + longitude);
                // intent.putExtra("city", "" + pickup_city);
                startActivityForResult(intent, 2545);

            }
        });
        ((TextView) findViewById(R.id.txt_home)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (!location_type.equalsIgnoreCase("Home")) {
                    location_type = "Home";
                    ((TextView) findViewById(R.id.txt_home)).setBackgroundResource(R.drawable.address_type_background_selected);
                    ((TextView) findViewById(R.id.txt_work)).setBackgroundResource(R.drawable.address_type_background_unselected);
                    ((TextView) findViewById(R.id.txt_other)).setBackgroundResource(R.drawable.address_type_background_unselected);
                }
            }
        });

        ((TextView) findViewById(R.id.txt_work)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (!location_type.equalsIgnoreCase("Work")) {
                    location_type = "Work";
                    ((TextView) findViewById(R.id.txt_home)).setBackgroundResource(R.drawable.address_type_background_unselected);
                    ((TextView) findViewById(R.id.txt_work)).setBackgroundResource(R.drawable.address_type_background_selected);
                    ((TextView) findViewById(R.id.txt_other)).setBackgroundResource(R.drawable.address_type_background_unselected);
                }
            }
        });

        ((TextView) findViewById(R.id.txt_other)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (!location_type.equalsIgnoreCase("Other")) {
                    location_type = "Other";
                    ((TextView) findViewById(R.id.txt_home)).setBackgroundResource(R.drawable.address_type_background_unselected);
                    ((TextView) findViewById(R.id.txt_work)).setBackgroundResource(R.drawable.address_type_background_unselected);
                    ((TextView) findViewById(R.id.txt_other)).setBackgroundResource(R.drawable.address_type_background_selected);
                }
            }
        });


    }

    public boolean validation() {
        Boolean valid = true;
        if (Utils.isEmpty(edtAddress1.getText().toString())) {
            txtWrapperAddress1.setError(getString(R.string.empty_field));
            valid = false;
        }
        return valid;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int i) {
                ((LinearLayout) findViewById(R.id.lyout_addressbar)).setVisibility(View.GONE);
            }
        });

        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                if (!isDrop) {
                    latitude = cameraPosition.target.latitude;

                    // Getting longitude of the current location
                    longitude = cameraPosition.target.longitude;
                    isDrop = false;
                    try {
                        Geocoder geocoder = new Geocoder(AddAddressActivity.this, Locale.getDefault());
                        List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
                        mMap.clear();
                        ((TextView) findViewById(R.id.txt_address)).setText(addresses.get(0).getAddressLine(0));

                        ((TextView) findViewById(R.id.txt_location)).setText(addresses.get(0).getAddressLine(0));
                        ((LinearLayout) findViewById(R.id.lyout_addressbar)).setVisibility(View.VISIBLE);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    lat = cameraPosition.target.latitude;
                    lan = cameraPosition.target.longitude;

                    try {
                        Geocoder geocoder = new Geocoder(AddAddressActivity.this, Locale.getDefault());
                        List<Address> addresses = geocoder.getFromLocation(lat, lan, 2);
                        drop_city = addresses.get(0).getLocality();
                        if (drop_city == null) {
                            drop_city = addresses.get(1).getLocality();
                        }
                        ((TextView) findViewById(R.id.txt_address)).setText(addresses.get(0).getAddressLine(0));

                        ((TextView) findViewById(R.id.txt_location)).setText(addresses.get(0).getAddressLine(0));
                        ((LinearLayout) findViewById(R.id.lyout_addressbar)).setVisibility(View.VISIBLE);
                        /*((TextView) findViewById(R.id.txt_address)).setText(addresses.get(0).getAddressLine(0) + ", " +
                                addresses.get(0).getAddressLine(1) + ", " + addresses.get(0).getAddressLine(2));
//                                ((LinearLayout) findViewById(R.id.lyout_addressbar)).setVisibility(View.VISIBLE);
                        ((LinearLayout) findViewById(R.id.lyout_addressbar)).setVisibility(View.VISIBLE);
//                                ((LinearLayout) findViewById(R.id.lyout_droplocation)).setVisibility(View.VISIBLE);
//                                ((TextView) findViewById(R.id.btn_get_distance)).setVisibility(View.VISIBLE);*/
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
              /*  latitude = cameraPosition.target.latitude;

                // Getting longitude of the current location
                longitude = cameraPosition.target.longitude;

                try {
                    Geocoder geocoder = new Geocoder(AddAddressActivity.this, Locale.getDefault());
                    List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
                    mMap.clear();
                    ((TextView) findViewById(R.id.txt_address)).setText(addresses.get(0).getAddressLine(0));

                    ((TextView) findViewById(R.id.txt_location)).setText(addresses.get(0).getAddressLine(0));
                    ((LinearLayout) findViewById(R.id.lyout_addressbar)).setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                }*/
            }

        });

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
        getLocation();
    }

    public void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            LoadMap(location);


//            LatLng sydney = new LatLng(location.getLatitude(), location.getLongitude());


        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private void LoadMap(Location location) {
        if (location != null) {

            latitude = location.getLatitude();

            // Getting longitude of the current location
            longitude = location.getLongitude();
            latti = latitude;
            longi = longitude;
            Geocoder gcd = new Geocoder(this, Locale.getDefault());
            List<Address> city = null;
            try {
                city = gcd.getFromLocation(latti, longi, 1);
                if (city.size() > 0) {
                    edtcity = city.get(0).getLocality();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }


            float speed = location.getSpeed();

            // Creating a LatLng object for the current location
            LatLng latLng = new LatLng(latitude, longitude);

            // Showing the current location in Google Map
            CameraPosition camPos = new CameraPosition.Builder()
                    .target(new LatLng(latitude, longitude))
                    .zoom(18)
                    .bearing(location.getBearing())
                    .tilt(0)
                    .build();
            CameraUpdate camUpd3 = CameraUpdateFactory.newCameraPosition(camPos);
            mMap.animateCamera(camUpd3);
            try {
                //        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 2);
                mMap.clear();
//            mMap.addMarker(new MarkerOptions().position(latLng).title(addresses.get(0).getAddressLine(0)+", "+
//                    addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2)));

                ((TextView) findViewById(R.id.txt_address)).setText(addresses.get(0).getAddressLine(0));
                ((TextView) findViewById(R.id.txt_location)).setText(addresses.get(0).getAddressLine(0));
                ((LinearLayout) findViewById(R.id.lyout_addressbar)).setVisibility(View.VISIBLE);

//
                //            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//            mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
//            locationText.setText(locationText.getText() + "\n"+addresses.get(0).getAddressLine(0)+", "+
//                    addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //     getLocation();
            Toast.makeText(this, "Unable to get location try again later", Toast.LENGTH_SHORT).show();

        }
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng sydney = new LatLng(location.getLatitude(), location.getLongitude());
        LoadMap(location);
    }

    //mLastLocation = location;


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2545) {
            if (resultCode == RESULT_OK) {

//                 isReady = false;
                drop_address = data.getStringExtra("address");
                lat = (double) data.getDoubleExtra("lat", 0.0);
                lan = (double) data.getDoubleExtra("lan", 0.0);
                Geocoder gcd = new Geocoder(AddAddressActivity.this, Locale.getDefault());
                List<Address> addresses = null;
                try {
                    addresses = gcd.getFromLocation(lat, lan, 2);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                latti = lat;
                longi = lan;

                if (addresses.size() > 0) {

                    if (addresses.get(1).getLocality() != null) {
                        System.out.println(addresses.get(1).getLocality());
                        drop_city = addresses.get(1).getLocality();
                        edtcity = drop_city;

                        if (drop_city.contains(","))
                            drop_city = addresses.get(1).getLocality();

                    } else {

                        Toast.makeText(this, "Service not available at this point.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (drop_city == null) {

                        if (addresses.get(0).getLocality() != null) {
                            if (addresses != null && addresses.size() > 0)
                                drop_city = addresses.get(0).getLocality();
                        } else {

                            Toast.makeText(this, "Service not available at this point.", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                } else {

                    Toast.makeText(this, "Service not available at this point.", Toast.LENGTH_SHORT).show();
                    return;
                }
//                CalculateDistance();


//                Toast.makeText(this, drop_city, Toast.LENGTH_SHORT).show();
                CameraPosition camPos = new CameraPosition.Builder()
                        .target(new LatLng(lat, lan))
                        .zoom(18)
                        .tilt(0)
                        .build();
                isDrop = true;
                CameraUpdate camUpd3 = CameraUpdateFactory.newCameraPosition(camPos);
                mMap.animateCamera(camUpd3);
                destinationLatLong = new LatLng(lat, lan);
                mMap.clear();
                ((ImageView) findViewById(R.id.imageView13)).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.txt_location)).setText(drop_address);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("place", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
                Log.d("TAG", "cancel result call from drop address");
            }
        }

    }

    public void addUserAddress(String phoneNumber) {
        //Create handle for the RetrofitInstance interface
        // progressbarShow();
        ApiInstant service = BaseRetrofit.getRetrofitInstance().create(ApiInstant.class);
        Log.d("TAG", "addUserAddress: " + phoneNumber + "       " + latti + "      " + longi);
        Call<Login> call = service.addUserAddress(phoneNumber, edtAddress1.getText().toString(), edtcity, latti, longi, location_type);
        //android_device_id
        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if (response.isSuccessful()) {
                    // Toast.makeText(AddAddressActivity.this, "dfgksjfgskdjf", Toast.LENGTH_SHORT).show();


                    Intent intent = new Intent(AddAddressActivity.this, AddressBookActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    //showProgressDialog();
                    Toast.makeText(getApplicationContext(), "Something went wrong...", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                //   prigressBarHide();
                Toast.makeText(getApplicationContext(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}