package com.viamart.customer.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

import com.google.gson.JsonObject;
import com.viamart.customer.R;
import com.viamart.customer.base_util_premission.BaseActivity;
import com.viamart.customer.model.Login;
import com.viamart.customer.model.OTPSend;
import com.viamart.customer.retrofit.ApiInstant;
import com.viamart.customer.retrofit.BaseRetrofit;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpVerficationActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.otpSubmit)
    AppCompatButton otpSubmit;
    @BindView(R.id.edtOTP)
    AppCompatEditText edtOTP;
    SharedPreferences sh;
    String phoneNumber, android_device_id;
    private Context context = this;
    String newString;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verfication);
        ButterKnife.bind(this);
        android_device_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        otpSubmit.setOnClickListener(this);
        sh = getSharedPreferences("MySharedPref", Context.MODE_APPEND);
        phoneNumber = sh.getString("phoneNumber", "");
        newString=sh.getString("access_token","");

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.otpSubmit) {
            String otp = edtOTP.getText().toString();
            if (otp.isEmpty()) {
                Toast.makeText(this, "Enter you OTP code.", Toast.LENGTH_SHORT).show();
            } else {
                postDataInOTP(phoneNumber, otp);
            }
            /*
             * Check in OTP API is true status come then oprn  DrawerProfileActivity
             * other wise oprn registation screen
             *
             * */
        }
    }

    /*
     * Post OTP on server with number
     * */
    private void postDataInOTP(String phone, String otp) {
        progressbarShow();
        JsonObject obj = new JsonObject();
        obj.addProperty("method", "checkOtp");
        JsonObject payerReg = new JsonObject();
        payerReg.addProperty("customer_phone", phoneNumber);
        payerReg.addProperty("login_token", otp);
        obj.add("body", payerReg);
        ApiInstant service = BaseRetrofit.getRetrofitInstance().create(ApiInstant.class);
        Call<OTPSend> call = service.OtpSendData(obj);
        call.enqueue(new Callback<OTPSend>() {
            @Override
            public void onResponse(@NotNull Call<OTPSend> call, @NotNull Response<OTPSend> response) {
                prigressBarHide();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        OTPSend otpSend = response.body();
                        int registerUSer = otpSend.getCheckOtp().getData().getIsRegister();
                        if (registerUSer == 0) {
                            Intent intent = new Intent(OtpVerficationActivity.this, RegistrationActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(OtpVerficationActivity.this, DrawerProfileActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                } else {
                    prigressBarHide();
                    Toast.makeText(getApplicationContext(), "Something went wrong...", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<OTPSend> call, Throwable t) {
                prigressBarHide();
                Toast.makeText(getApplicationContext(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        //LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        // LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                edtOTP.setText(message);
                // message is the fetching OTP
            }
        }
    };
}