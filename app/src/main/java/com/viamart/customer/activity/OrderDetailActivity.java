package com.viamart.customer.activity;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.viamart.customer.R;
import com.viamart.customer.adapter.OrderDetailAdapter;
import com.viamart.customer.base_util_premission.BaseActivity;
import com.viamart.customer.model.Order;
import com.viamart.customer.retrofit.ApiInstant;
import com.viamart.customer.retrofit.BaseRetrofit;

import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailActivity extends BaseActivity {

    @BindView(R.id.rv_orderList)
    RecyclerView rv_orderList;
    OrderDetailAdapter orderListAdapter;
    int bookingId;
    SharedPreferences sh;
    String phoneNumber;

    private long mLastClickTime = 0;

    TextView txtStoreName, txtDropName, txt_deliveryInfo, txtShopAddress, txtDropAddress, txt_totalPrice, txt_deliveryChargesPrice, txt_discountPrice, txt_taxPrice, txt_paymentType, txt_finalPrice;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        bookingId = getIntent().getIntExtra("OrderId", 1);
        sh = getSharedPreferences("MySharedPref", MODE_PRIVATE);
        phoneNumber = sh.getString("phoneNumber", "");

        txtStoreName = findViewById(R.id.txtStoreName);
        txtDropName = findViewById(R.id.txtDropName);
        txtShopAddress = findViewById(R.id.txtShopAddress);
        txt_deliveryInfo = findViewById(R.id.txt_deliveryInfo);
        txtDropAddress = findViewById(R.id.txtDropAddress);
        txt_totalPrice = findViewById(R.id.txt_totalPrice);
        txt_deliveryChargesPrice = findViewById(R.id.txt_deliveryChargesPrice);
        txt_discountPrice = findViewById(R.id.txt_discountPrice);
        txt_taxPrice = findViewById(R.id.txt_taxPrice);
        txt_paymentType = findViewById(R.id.txt_paymentType);
        txt_finalPrice = findViewById(R.id.txt_finalPrice);
        getOrder(phoneNumber);
        ((ImageView) findViewById(R.id.imgBackOrderHIstory)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }

                mLastClickTime = SystemClock.elapsedRealtime();
                onBackPressed();
            }
        });
    }

    public void getOrder(String phoneNumber) {
        progressbarShow();
        ApiInstant service = BaseRetrofit.getRetrofitInstance().create(ApiInstant.class);
        Call<Order> call = service.getOrders(phoneNumber);
        call.enqueue(new Callback<Order>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                prigressBarHide();

                if (response.isSuccessful()) {
                    Order order = response.body();

                    List<Order.Data> orderlist = order.getOrder();

                    txtStoreName.setText(orderlist.get(0).getStoreName());
                    txtShopAddress.setText(orderlist.get(0).getStoreAddress1() + " " + orderlist.get(0).getStoreAddress2() + " " + orderlist.get(0).getStoreCityName());
                    txtStoreName.setText(orderlist.get(0).getStoreName());
                    txt_deliveryInfo.setText(orderlist.get(0).getStatus() + " on " + orderlist.get(0).getBookingDate() + " " + orderlist.get(0).getOrderTime());
                    if (orderlist.get(0).getBookingAmount() != null) {
                        txt_totalPrice.setText(Double.toString(orderlist.get(0).getBookingAmount()));
                    }
                    txt_deliveryChargesPrice.setText(Double.toString(orderlist.get(0).getDeliveryCharge()));
                    txt_discountPrice.setText(Double.toString(orderlist.get(0).getDiscount()));
                    txt_taxPrice.setText(Double.toString(orderlist.get(0).getTax()));
                    txt_paymentType.setText("Paid Via " + orderlist.get(0).getPaymentType());
                    txt_finalPrice.setText(Double.toString((orderlist.get(0).getTotalAmount())));
                    orderListAdapter = new OrderDetailAdapter(orderlist, getApplicationContext());

                    rv_orderList = findViewById(R.id.rv_orderList);
                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);

                    rv_orderList.setLayoutManager(mLayoutManager);
                    rv_orderList.setItemAnimator(new DefaultItemAnimator());
                    rv_orderList.setAdapter(orderListAdapter);

                } else {
                    Toast.makeText(getApplicationContext(), "" + response.message(), Toast.LENGTH_SHORT).show();
                    Log.d("TAG", "onResponse: " + response.body());

                }
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                prigressBarHide();
                Toast.makeText(getApplicationContext(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
