package com.viamart.customer.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.viamart.customer.R;
import com.viamart.customer.adapter.OrderListAdapter;
import com.viamart.customer.base_util_premission.BaseActivity;
import com.viamart.customer.model.Order;
import com.viamart.customer.retrofit.ApiInstant;
import com.viamart.customer.retrofit.BaseRetrofit;

import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderListActivity extends BaseActivity {


    @BindView(R.id.recyclerOrderList)
    RecyclerView recyclerOrderList;
    OrderListAdapter orderListAdapter;
    LinearLayout linNoData;
    SharedPreferences sh;
    String phoneNumber;

    private long mLastClickTime = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        linNoData = findViewById(R.id.linNoData);
        sh = getSharedPreferences("MySharedPref", MODE_PRIVATE);
        phoneNumber = sh.getString("phoneNumber", "");

        getOrder(phoneNumber);
        ((ImageView) findViewById(R.id.imgBackOrderHIstory)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }

                mLastClickTime = SystemClock.elapsedRealtime();
                onBackPressed();
            }
        });

    }

    public void getOrder(String phoneNumber) {
        progressbarShow();
        ApiInstant service = BaseRetrofit.getRetrofitInstance().create(ApiInstant.class);
        Call<Order> call = service.getOrders(phoneNumber);
        call.enqueue(new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                prigressBarHide();

                if (response.isSuccessful()) {
                    Order order = response.body();

                    List<Order.Data> orderlist = order.getOrder();
                    if (orderlist != null) {
                        if (orderlist.size() > 0) {
                            orderListAdapter = new OrderListAdapter(orderlist, getApplicationContext(), new OrderListAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent intent = new Intent(OrderListActivity.this, OrderDetailActivity.class).putExtra("OrderId", orderlist.get(position).getBookingId());
                                    startActivity(intent);
                                }
                            });
                            recyclerOrderList = findViewById(R.id.recyclerOrderList);
                            RecyclerView.LayoutManager mLayoutManagershopName = new LinearLayoutManager(getApplicationContext());
                            recyclerOrderList.setLayoutManager(mLayoutManagershopName);
                            recyclerOrderList.setAdapter(orderListAdapter);
                        } else {

                            linNoData.setVisibility(View.VISIBLE);
                        }
                    } else {

                        linNoData.setVisibility(View.VISIBLE);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "" + response.message(), Toast.LENGTH_SHORT).show();
                    Log.d("TAG", "onResponse: " + response.body());

                }
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                prigressBarHide();
                Toast.makeText(getApplicationContext(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });
    }


}
