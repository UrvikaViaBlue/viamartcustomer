package com.viamart.customer.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.viamart.customer.R;
import com.viamart.customer.model.User;
import com.viamart.customer.retrofit.ApiInstant;
import com.viamart.customer.retrofit.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {
    SharedPreferences sh;
    String phoneNumber, deviceID;
    TextView txt_phn, txt_Name;
    ImageView cir_image;
    RelativeLayout rl_addressBook, rl_order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        txt_phn = findViewById(R.id.txt_phn);
        txt_Name = findViewById(R.id.txt_Name);
        cir_image = findViewById(R.id.cir_image);
        rl_addressBook = findViewById(R.id.rl_addressBook);
        rl_order = findViewById(R.id.rl_order);

        sh = getSharedPreferences("MySharedPref", MODE_PRIVATE);
        phoneNumber = sh.getString("phoneNumber", "");


        deviceID = sh.getString("Device_ID", "");

        GetUserData(phoneNumber, deviceID);
        rl_addressBook.setOnClickListener(this);
        rl_order.setOnClickListener(this);
        Log.d("TAG", "onCreate: "+phoneNumber+"     "+deviceID);

    }


    public void GetUserData(String phoneNumber, String deviceID) {
        //Create handle for the RetrofitInstance interface
        try {
            ApiInstant service = BaseRetrofit.getRetrofitInstance().create(ApiInstant.class);
            Call<User> call = service.GetUserData(phoneNumber, deviceID);
            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if (response.isSuccessful()) {
                        User userdata = response.body();
                        if(userdata != null) {
                            txt_phn.setText(phoneNumber);
                            Log.d("Userdata", "onResponse: " + response.body());
                            txt_Name.setText(userdata.getData().getUserName());
                            Toast.makeText(getApplicationContext(), "" + userdata.getData().getUserImage(), Toast.LENGTH_SHORT).show();
                            Log.d("dasfdau", "onResponse: "+userdata.getData().getUserImage());
                            if (userdata.getData().getUserImage() != null) {

                                Glide.with(getApplicationContext())
                                        .load(userdata.getData().getUserImage())
                                        .into(cir_image);

                            } else {

                            }
                        }else {
                            Toast.makeText(getApplicationContext(), "inn process", Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        //  Toast.makeText(getApplicationContext(), "inn process", Toast.LENGTH_SHORT).show();

                    }
                }


                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    // Toast.makeText(getApplicationContext(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_addressBook:
                Intent intent = new Intent(ProfileActivity.this, AddressBookActivity.class).putExtra("FromProfile",true);
                startActivity(intent);
                break;
            case R.id.rl_order:
                Intent intentOrder = new Intent(ProfileActivity.this, OrderListActivity.class);
                startActivity(intentOrder);
                break;
            default:
        }
    }
}
