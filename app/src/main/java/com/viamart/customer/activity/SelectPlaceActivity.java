package com.viamart.customer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.viamart.customer.R;
import com.viamart.customer.adapter.RecyclerViewAdapter;
import com.viamart.customer.interfaceData.RecycleItemClickListner;
import com.viamart.customer.retrofit.ApiInstant;

import java.util.ArrayList;
import java.util.List;

public class SelectPlaceActivity extends AppCompatActivity implements RecycleItemClickListner {

    private long mLastClickTime = 0;

    double lat, lan;
    String MainName, place, placeID;

    List<String> place_list = new ArrayList<>();
    List<Integer> img = new ArrayList<>();
    List<String> address = new ArrayList<>();
    List<Double> latti = new ArrayList<>();
    List<Double> longi = new ArrayList<>();
    RecyclerViewAdapter recyclerViewAdapter;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView rv_addresses;
    public static ProgressBar progressbar_searchLocation;
    TextView txtTitle_SelectPlaceAct;

    String location_type = "Home", location_type_drop = "Home";
    //    Toolbar toolbar;
    String latlan;

    private void showFailedView(boolean show) {
        View lyt_failed = findViewById(R.id.lyt_failed);
        if (show) {
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            lyt_failed.setVisibility(View.GONE);
        }
        findViewById(R.id.failed_retry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (ApiInstant.checkInternetConnection(SelectPlaceActivity.this)) {
                    lyt_failed.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.new_activity_select_place);
        if (!ApiInstant.checkInternetConnection(SelectPlaceActivity.this)) {
            showFailedView(true);
        }

        txtTitle_SelectPlaceAct = findViewById(R.id.txtTitle_SelectPlaceAct);

        ((ImageView) findViewById(R.id.imgBack_SelectPlactAct)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // mis-clicking prevention, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }

                mLastClickTime = SystemClock.elapsedRealtime();
                onBackPressed();
            }
        });

        latlan = getIntent().getStringExtra("latlan");

        AdpOfDesinationAddress itemArrayAdapter = new AdpOfDesinationAddress(this, R.layout.autocomplete_list_item, android.R.id.text1);
        ListView listView = (ListView) findViewById(R.id.list_location);
        listView.setAdapter(itemArrayAdapter);

        AdpOfDesinationAddress.placeList = new ArrayList<>();

        PoppulateList();

        progressbar_searchLocation = (ProgressBar) findViewById(R.id.progressbar_searchLocation);

        rv_addresses = findViewById(R.id.rv_addresses);
        AddAddressActivity.recycleItemClickListner = this;

        layoutManager = new LinearLayoutManager(SelectPlaceActivity.this);
        rv_addresses.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        recyclerViewAdapter = new RecyclerViewAdapter(SelectPlaceActivity.this, place_list, address);
        rv_addresses.setAdapter(recyclerViewAdapter);

//        ((AutoCompleteTextView) findViewById(R.id.autoCompleteTextView)).setAdapter(new AdpOfDesinationAddress(this, R.layout.autocomplete_list_item, android.R.id.text1));
        ((EditText) findViewById(R.id.autoCompleteTextView)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                if (progressbar_searchLocation != null) {
                    progressbar_searchLocation.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                itemArrayAdapter.getFilter(s.toString(), latlan);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                place = AdpOfDesinationAddress.placeList.get(position).getDescription();

                placeID = AdpOfDesinationAddress.placeList.get(position).getPlace_id();

                MainName = AdpOfDesinationAddress.placeList.get(position).getDescription();

                GeocodingLocation locationAddress = new GeocodingLocation();
                locationAddress.getAddressFromLocation(place,
                        getApplicationContext(), new GeocoderHandler());

            }
        });

    }

    private void PoppulateList() {
        String[] place1 = {"Work", "Home", "Other"};
        double[] lat = {21.248574, 21.248574, 21.248574};
        double[] lan = {72.881243, 72.881243, 72.881243};
        int[] img1 = {R.drawable.ic_work, R.drawable.ic_home, R.drawable.ic_other};
        String[] address1 = {"501 visharad Comp.,Opp. Loha Bhahvan,Income Tax, Ahmedabad-380024",
                "502 visharad Comp.,Opp. Loha Bhahvan,Income Tax, Ahmedabad-380024",
                "503 visharad Comp.,Opp. Loha Bhahvan,Income Tax, Ahmedabad-380024"};

        for (String pl : place1) {
            place_list.add(pl);
        }
        for (Integer pl : img1) {
            img.add(pl);
        }
        for (String pl : address1) {
            address.add(pl);
        }
        for (double pl : lat) {
            latti.add(pl);
        }
        for (double pl : lan) {
            longi.add(pl);
        }

    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    lat = bundle.getDouble("latitude");
                    lan = bundle.getDouble("langi");
                    Intent intent = new Intent();
                    intent.putExtra("address", "" + place);
                    intent.putExtra("lat", lat);
                    intent.putExtra("lan", lan);
                    setResult(RESULT_OK, intent);
                    finish();
                    break;
                default:
                    locationAddress = null;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(View view, int position) {
//        cv_dialog.setVisibility(View.GONE);
        String add = address.get(position);
        lat = latti.get(position);
        lan = longi.get(position);
        Intent intent = new Intent();
        intent.putExtra("address", "" + add);
        intent.putExtra("lat", lat);
        intent.putExtra("lan", lan);
        setResult(RESULT_OK, intent);
        finish();
    }


}
