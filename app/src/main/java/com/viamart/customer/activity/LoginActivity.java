package com.viamart.customer.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.viamart.customer.R;
import com.viamart.customer.adapter.CountrySpinnerAdapter;
import com.viamart.customer.base_util_premission.BaseActivity;
import com.viamart.customer.base_util_premission.Constants;
import com.viamart.customer.base_util_premission.PromptDialog;
import com.viamart.customer.model.Login;
import com.viamart.customer.model.OTPGet;
import com.viamart.customer.retrofit.ApiInstant;
import com.viamart.customer.retrofit.BaseRetrofit;
import com.viamart.customer.utils.LocationTrack;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    @BindView(R.id.edtPhoneNumber)
    AppCompatEditText edtPhoneNumber;
    @BindView(R.id.btnSubmit)
    AppCompatButton btnSubmit;
    String[] countryNames = {"+91", "+123", "+5", "+23", "+46", "+153"};
    BaseRetrofit baseRetrofit;
    boolean connected = false;
    private Context context = this;
    String android_device_id;
    private LocationManager mLocationManager;
    double latitude, longitude;
    LocationTrack locationTrack;
    @BindView(R.id.chk_termsCondition)
    AppCompatCheckBox chk_termsCondition;
    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();

    private final static int ALL_PERMISSIONS_RESULT = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        btnSubmit.setOnClickListener(this);
       // spinner.setOnItemSelectedListener(this);
        CountrySpinnerAdapter countrySpinnerAdapter = new CountrySpinnerAdapter(getApplicationContext(), countryNames);
       // spinner.setAdapter(countrySpinnerAdapter);
        android_device_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
           /* mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1,
                    1, mLocationListener);*/
        } else {
            ActivityCompat.requestPermissions((Activity) context, new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    1);
        }


    }

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }


        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();

        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (Object perms : permissionsToRequest) {
                    if (!hasPermission((String) perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions((String[]) permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(LoginActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        locationTrack.stopListener();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnSubmit) {

            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                    connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
                //we are connected to a network
                connected = true;
                String number = edtPhoneNumber.getText().toString();
                if (number.isEmpty()) {
                    Toast.makeText(this, "Enter your mobile number.", Toast.LENGTH_SHORT).show();
                } else if (number.length() > 10) {
                    Toast.makeText(this, "Enter your valid mobile number.", Toast.LENGTH_SHORT).show();
                } else if (number.length() < 10) {
                    Toast.makeText(this, "Enter your valid mobile number.", Toast.LENGTH_SHORT).show();
                }else if(!chk_termsCondition.isChecked()){
                    Toast.makeText(context, "Please accept terms and condition.", Toast.LENGTH_SHORT).show();
                }
                else {
                    locationTrack = new LocationTrack(LoginActivity.this);
                    permissionsToRequest = findUnAskedPermissions(permissions);
                    //get the permissions we have asked for before but are not granted..
                    //we will store this in a global list to access later.


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (permissionsToRequest.size() > 0)
                            requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
                    }

                    if (locationTrack.canGetLocation()) {
                        longitude = locationTrack.getLongitude();
                        latitude = locationTrack.getLatitude();
                        LogingAPi(number);
                    } else {
                        locationTrack.showSettingsAlert();
                    }

                }
            } else {
                connected = false;
                Toast.makeText(this, "Check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void LogingAPi(String phoneNumber) {
        //Create handle for the RetrofitInstance interface
        progressbarShow();
        ApiInstant service = BaseRetrofit.getRetrofitInstance().create(ApiInstant.class);
        JsonObject obj = new JsonObject();
        obj.addProperty("method", "userSignin");
        JsonObject payerReg = new JsonObject();
        payerReg.addProperty("customer_phone", phoneNumber);
        payerReg.addProperty("access_token", "");
        obj.add("body", payerReg);
        Call<Login> call = service.LoginUser(obj);
        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                prigressBarHide();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Login login = response.body();
                        login.getUserSignin().getData();
                        String access_token = "";
                        if (login.getUserSignin().getData().getAccessToken() != null) {
                            access_token = login.getUserSignin().getData().getAccessToken();
                        }
                        // OTPAPi(phoneNumber);
                        SharedPreferences sharedPreferences
                                = getSharedPreferences("MySharedPref",
                                MODE_PRIVATE);
                        SharedPreferences.Editor myEdit
                                = sharedPreferences.edit();
                        myEdit.putString("phoneNumber", phoneNumber);
                        myEdit.putString("Device_ID", android_device_id);
                        myEdit.putString("access_token", access_token);
                        myEdit.apply();
                        Intent intent = new Intent(LoginActivity.this, OtpVerficationActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    // OTPAPi(phoneNumber);
                    showProgressDialog();
                    Toast.makeText(getApplicationContext(), "Something went wrong...", Toast.LENGTH_SHORT).show();
                    SharedPreferences sharedPreferences
                            = getSharedPreferences("MySharedPref",
                            MODE_PRIVATE);
                    SharedPreferences.Editor myEdit
                            = sharedPreferences.edit();
                    myEdit.putString("phoneNumber", phoneNumber);
                    Intent intent = new Intent(LoginActivity.this, OtpVerficationActivity.class);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                prigressBarHide();
                Toast.makeText(getApplicationContext(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /*
     * Get OTP on number
     * */
    public void OTPAPi(String phoneNumber) {
        //Create handle for the RetrofitInstance interface
       /* ApiInstant service = BaseRetrofit.getRetrofitInstance().create(ApiInstant.class);
        Call<OTPGet> call = service.OtpData(phoneNumber, android_device_id);
        call.enqueue(new Callback<OTPGet>() {
            @Override
            public void onResponse(Call<OTPGet> call, Response<OTPGet> response) {
                if (response.isSuccessful()) {
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Something went wrong...", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<OTPGet> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });*/
    }
}