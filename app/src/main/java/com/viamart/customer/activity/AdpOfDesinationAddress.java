package com.viamart.customer.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filterable;

import com.viamart.customer.model.AddressDataWithPlace;
import com.viamart.customer.retrofit.ApiInstant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by ms-02 on 4/11/17.
 */
public class AdpOfDesinationAddress extends ArrayAdapter<String> implements Filterable {

    public  static List<AddressDataWithPlace> placeList = new ArrayList<>();
    private static final String LOG_TAG = "AdpOfDesinationAddress";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    //private static final String PLACE_DETAILS = "https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJYxUdQVlO4DsRQrA4CSlYRf4&key=AIzaSyCLZcxTxWWEAqqK71XKRmTG39g8N0eg1Ak";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    Context ctx;

    public AdpOfDesinationAddress(Context context, int resource, int text1) {
        super(context, resource, text1);

    }

    @Override
    public int getCount() {
        return placeList.size();
    }

    @Override
    public String getItem(int position) {
        return placeList.get(position).getDescription();
    }

    public void getFilter(String text, String latlan) {

        getList(text, latlan);
        Log.e("place-->>", placeList.size() + "");

    }

    @SuppressLint("LongLogTag")
    public static List<AddressDataWithPlace> autoComplete(String input) {

        return placeList;
    }

    private void getList(String input, String latlan) {
        class RetrieveFeedTask extends AsyncTask<String, Void, List<AddressDataWithPlace>> {

            private Exception exception;

            protected List<AddressDataWithPlace> doInBackground(String... urls) {
                HttpURLConnection conn = null;
                final StringBuilder jsonResults = new StringBuilder();
                try {
                    StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
                    sb.append("?key=" + ApiInstant.GOOGLE_MAP_API_KEY_FOR_PLACE);
                    sb.append("&components=country:" + ApiInstant.countrCode);
                    sb.append("&location=" + latlan);
                    sb.append("&radius=5000");
                    sb.append("&sensor=true");
                    sb.append("&input=" + URLEncoder.encode(input, "utf8"));
                    Log.d(LOG_TAG, sb.toString());
                    URL url = new URL(sb.toString());
                    conn = (HttpURLConnection) url.openConnection();
                    InputStreamReader in = new InputStreamReader(conn.getInputStream());
                    int read;
                    char[] buff = new char[1024];
                    while ((read = in.read(buff)) != -1) {
                        jsonResults.append(buff, 0, read);
                    }
                } catch (MalformedURLException e) {
                    Log.e(LOG_TAG, "Error processing Places API URL", e);
                    return new ArrayList<AddressDataWithPlace>();
                } catch (IOException e) {
                    Log.e(LOG_TAG, "Error connecting to Places API", e);
                    return new ArrayList<AddressDataWithPlace>();
                } finally {
                    if (conn != null) {
                        conn.disconnect();
                    }
                }

                try {
                    JSONObject jsonObj = new JSONObject(jsonResults.toString());
                    JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");
                  placeList = new ArrayList<AddressDataWithPlace>();

                    for (int i = 0; i < predsJsonArray.length(); i++) {

                        AddressDataWithPlace addressDataWithPlace2 = new AddressDataWithPlace();
                        addressDataWithPlace2.setDescription(predsJsonArray.getJSONObject(i).getString("description"));
                        addressDataWithPlace2.setPlace_id(predsJsonArray.getJSONObject(i).getString("place_id"));
                        if (placeList == null) {
                          placeList = new ArrayList<>();
                        }
                        placeList.add(addressDataWithPlace2);
                    }
                } catch (JSONException e) {
                    Log.e(LOG_TAG, "Cannot process JSON results", e);
                }

                return placeList;
            }

            protected void onPostExecute(List<AddressDataWithPlace> feed) {
                notifyDataSetChanged();

                Log.e("size==>>", feed.size() + "");
            }
        }
        new RetrieveFeedTask().execute("");
    }

    private void notifyAdapter() {

        notifyDataSetChanged();
    }

}
