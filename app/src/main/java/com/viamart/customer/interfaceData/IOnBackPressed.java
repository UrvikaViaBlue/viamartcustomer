package com.viamart.customer.interfaceData;

public interface IOnBackPressed {
    boolean onBackPressed();
}
