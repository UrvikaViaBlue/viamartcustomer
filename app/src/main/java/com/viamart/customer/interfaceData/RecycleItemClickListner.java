package com.viamart.customer.interfaceData;

import android.view.View;

public interface RecycleItemClickListner {
        void onItemClick(View view, int position);
}
