package com.viamart.customer.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.viamart.customer.R;
import com.viamart.customer.model.StoreCategory;

import java.util.List;

public class VariantItemAdapter extends RecyclerView.Adapter<VariantItemAdapter.MyViewHolder> {

    Context context;
    List<StoreCategory.VariantName> storeProductList;
    private RadioGroup lastCheckedRadioGroup = null;
    private OnItemClickListener onItemClickListener;
    private int lastSelectedPosition = -1;

    public VariantItemAdapter(Context ctx, List<StoreCategory.VariantName> storeProduct, OnItemClickListener onItemClick) {
        this.context = ctx;
        this.storeProductList = storeProduct;
        this.onItemClickListener = onItemClick;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.variant_item_rv, parent, false);

        VariantItemAdapter.MyViewHolder viewHolder =
                new VariantItemAdapter.MyViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        StoreCategory.VariantName storeProductData = storeProductList.get(position);
        // RadioButton rb = new RadioButton(context);
        String variantData = String.format("%s                               %s  RS. ", storeProductData.getVariantName(), storeProductData.getProductPrice());
        // rb.setText(variantData);
        holder.radioGroup.setText(variantData);
        if (position == 0) {
            holder.radioGroup.setChecked(true);
        } else {
            holder.radioGroup.setChecked(false);
        }
        holder.radioGroup.setChecked(lastSelectedPosition == position);

       /* holder.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
              *//*  if (lastCheckedRadioGroup != null
                        && lastCheckedRadioGroup.getCheckedRadioButtonId()
                        != radioGroup.getCheckedRadioButtonId()
                        && lastCheckedRadioGroup.getCheckedRadioButtonId() != -1) {*//*


                    RadioButton radioButton = (RadioButton) radioGroup.findViewById(i);
                    String v2 = null;
                    if (radioButton.getText().toString().length() > 0) {
                        String selectedRbText = radioButton.getText().toString();
                        String[] splited = selectedRbText.split("\\s+");
                        String v1 = splited[0];
                        v2 = splited[1];
                        String v3 = splited[2];
                        Log.d("TAG", "onCheckedChanged: 2" + v2);
                        
                    }
                    onItemClickListener.onItemClick(v2, position);
                   *//* lastCheckedRadioGroup.clearCheck();
                }
                lastCheckedRadioGroup = radioGroup;*//*
            }
        });
        holder.radioGroup.addView(rb);*/
    }

    @Override
    public int getItemCount() {
        return storeProductList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        RadioButton radioGroup;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            radioGroup = itemView.findViewById(R.id.radioGroup);
            radioGroup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String radioData = radioGroup.getText().toString();
                    String[] splited = radioData.split("\\s+");
                    String v2 = splited[1];
                    onItemClickListener.onItemClick(v2, getAdapterPosition());
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(String view, int position);
    }
}
