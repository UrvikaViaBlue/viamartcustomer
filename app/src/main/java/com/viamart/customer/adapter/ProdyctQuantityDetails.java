package com.viamart.customer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.viamart.customer.R;
import com.viamart.customer.model.StoreCategory;
import com.viamart.customer.model.StoreProductDetails;

import java.util.List;

public class ProdyctQuantityDetails extends BaseAdapter {
    Context context;
    LayoutInflater inflter;
    List<StoreCategory.VariantName> varientsList;
    OnItemClickListener onItemClickListener;

    public ProdyctQuantityDetails(Context context, List<StoreCategory.VariantName> varients, OnItemClickListener onItemClick) {
        this.context = context;
        inflter = (LayoutInflater.from(context));
        this.onItemClickListener = onItemClick;
        this.varientsList = varients;
    }

    @Override
    public int getCount() {
        return varientsList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        StoreCategory.VariantName storeProductVariantName= varientsList.get(i);
        view = inflter.inflate(R.layout.custom_spinner_items, null);

        TextView names = (TextView) view.findViewById(R.id.textView);
        names.setText(storeProductVariantName.getVariantName());
        //onItemClickListener.onItemClickAddItem(v, position,Integer.parseInt(quantity));
        return view;
    }

    public interface OnItemClickListener {
        void onItemClickAddItem(View view, int position);
    }
}
