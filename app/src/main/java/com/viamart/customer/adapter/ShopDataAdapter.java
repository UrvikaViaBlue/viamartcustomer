package com.viamart.customer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.viamart.customer.R;
import com.viamart.customer.model.StoreDetails;

import java.text.DecimalFormat;
import java.util.List;

public class ShopDataAdapter extends RecyclerView.Adapter<ShopDataAdapter.MyViewHolder> {
    Context context;
    private OnItemClickListener onItemClickListener;
    private List<StoreDetails.StoreDetail> storeDataLists;

    public ShopDataAdapter(List<StoreDetails.StoreDetail> storeData, Context ctx, OnItemClickListener onItemClick) {
        this.context = ctx;
        this.onItemClickListener = onItemClick;
        storeDataLists = storeData;
    }



    @NonNull
    @Override
    public ShopDataAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shop_name, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopDataAdapter.MyViewHolder holder, int position) {
        StoreDetails.StoreDetail storeDataList = storeDataLists.get(position);
        holder.txtDescription.setText(storeDataList.getDescription());
        holder.txtStoreName.setText(storeDataList.getStoreName());
        //DecimalFormat precision = new DecimalFormat(String.valueOf());
        holder.develveryKm.setText(storeDataList.getDistance());


        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(v, position);
            }
        });
        if (storeDataList.getStoreImage() != null) {
            Glide.with(context).
                    load(storeDataList.getStoreImage()).
                    into(holder.imgShop);
        } else {
            holder.imgShop.setBackgroundResource(R.drawable.no_img_found);
        }
    }

    @Override
    public int getItemCount() {
        return storeDataLists.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout layout;
        TextView txtDescription, txtStoreName, develveryKm;
        AppCompatImageView imgShop;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            layout = itemView.findViewById(R.id.layout);
            txtDescription = itemView.findViewById(R.id.txtDescription);
            txtStoreName = itemView.findViewById(R.id.txtStoreName);
            develveryKm = itemView.findViewById(R.id.develveryKm);
            imgShop = itemView.findViewById(R.id.imgShop);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}
