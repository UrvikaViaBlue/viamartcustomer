package com.viamart.customer.adapter;

import android.content.Context;
import android.content.Intent;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.viamart.customer.R;
import com.viamart.customer.activity.AddAddressActivity;
import com.viamart.customer.model.Address;
import com.viamart.customer.retrofit.ApiInstant;
import com.viamart.customer.retrofit.BaseRetrofit;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressBookAdapter extends RecyclerView.Adapter<AddressBookAdapter.MyViewHolder> {
    Context context;
    OnItemClickListener onItemClickListener;
    List<Address.Data> storeDataLists;
    int position;
    Boolean Previousflag = false;

    public AddressBookAdapter(List<Address.Data> storeData, Context ctx, OnItemClickListener onItemClick) {
        this.context = ctx;
        this.onItemClickListener = onItemClick;
        storeDataLists = storeData;
    }


    @NonNull
    @Override
    public AddressBookAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_address_book, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull AddressBookAdapter.MyViewHolder holder, int position) {
        Address.Data storeDataList = storeDataLists.get(position);
        holder.txtAddressType.setText(storeDataList.getAddType());
        holder.txtAddressFull.setText(storeDataList.getAddress1());

        Geocoder gcd = new Geocoder(context, Locale.getDefault());
        List<android.location.Address> addresses = null;
        try {
            Log.d("latlong", "onBindViewHolder: " + storeDataList.getLongi() + storeDataList.getLati());
            addresses = gcd.getFromLocation(storeDataList.getLati(), storeDataList.getLongi(), 1);
            Log.d("TAG", "onBindViewHolder: adres : " + addresses);
            if (addresses.size() > 0) {
                holder.txtAddressLine2.setText(addresses.get(position).getAddressLine(0));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
//        holder.txtCity.setText(storeDataList.getCity());
        holder.imgmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //creating a popup menu
                PopupMenu popup = new PopupMenu(context, holder.imgmenu);
                //inflating menu from xml resource
                popup.inflate(R.menu.menu_scrolling);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_edit:
                                Previousflag = true;
                                Intent intent = new Intent(context, AddAddressActivity.class);
                                context.startActivity(new Intent(context, AddAddressActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).putExtra("Latitude", storeDataList.getLati()).putExtra("Longitude", storeDataList.getLongi()));
                                Log.d("TAG", "onMenuItemClick: " + storeDataList.getLati() + "     " + storeDataList.getLongi());
                                /*intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(intent);*/

                                return true;
                            case R.id.action_delete:
                                deleteAddress(position);
                                removeAt(position);
                                return true;

                            default:
                                return false;
                        }
                    }
                });
                //displaying the popup
                popup.show();

            }
        });

    /*    holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.equals(holder.imgEdit)) {
                    Intent intent = new Intent(context, AddAddressActivity.class);

                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            }
        });*/
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(v, position);

            }
        });
    }

    @Override
    public int getItemCount() {
        return storeDataLists.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout layout;
        ImageView imgmenu, imgEdit;
        TextView txtAddressType, txtAddressFull, txtAddressLine2, txtCity;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            layout = itemView.findViewById(R.id.layout);
            txtAddressType = itemView.findViewById(R.id.txtAddressType);
            txtAddressFull = itemView.findViewById(R.id.txtAddressFull);
            imgmenu = itemView.findViewById(R.id.imgmenu);
            txtAddressLine2 = itemView.findViewById(R.id.txtAddressLine2);
            // txtCity = itemView.findViewById(R.id.txtCity);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void removeAt(int position) {
        storeDataLists.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, storeDataLists.size());
    }

    private void deleteAddress(int position) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {

            try {
                ApiInstant service = BaseRetrofit.getRetrofitInstance().create(ApiInstant.class);
                Call<Address> call = service.removeuseraddress(storeDataLists.get(position).getId());
                call.enqueue(new Callback<Address>() {
                    @Override
                    public void onResponse(Call<Address> call, Response<Address> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(context, "Address Removed successfully", Toast.LENGTH_LONG).show();

                        } else {
                            Toast.makeText(context, "Try Agin", Toast.LENGTH_LONG).show();
                        }

                    }


                    @Override
                    public void onFailure(Call<Address> call, Throwable t) {
                        Log.d("TAG", "onFailure: " + t);
                        Toast.makeText(context, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            Toast.makeText(context, "Somthing went to wrong.", Toast.LENGTH_SHORT).show();
        }
    }

}