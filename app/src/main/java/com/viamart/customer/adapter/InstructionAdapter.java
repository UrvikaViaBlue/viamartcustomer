package com.viamart.customer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.viamart.customer.R;
import com.viamart.customer.model.StoreCategoryFilter;

import java.util.List;

public class InstructionAdapter extends RecyclerView.Adapter<InstructionAdapter.MyViewHolder> {

    Context context;
    List<StoreCategoryFilter.Datum> storeCategoryList;

    public InstructionAdapter(List<StoreCategoryFilter.Datum> datumList, Context ctx) {
        this.context = ctx;
        this.storeCategoryList = datumList;
    }

    @NonNull
    @Override
    public InstructionAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_instruction_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull InstructionAdapter.MyViewHolder holder, int position) {
        StoreCategoryFilter.Datum storeDataList = storeCategoryList.get(position);
        holder.txt_inst_offer.setText(storeDataList.getCategoryName());
    }

    @Override
    public int getItemCount() {
        return storeCategoryList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView txt_inst_offer;
        AppCompatImageView img_inst_offer;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_inst_offer = itemView.findViewById(R.id.txt_inst_offer);
        }
    }
}
