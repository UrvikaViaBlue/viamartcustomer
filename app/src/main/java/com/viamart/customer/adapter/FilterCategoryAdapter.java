package com.viamart.customer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.viamart.customer.R;

public class FilterCategoryAdapter extends RecyclerView.Adapter<FilterCategoryAdapter.MyViewHolder> {
    Context context;
    private RadioGroup lastCheckedRadioGroup = null;

    public FilterCategoryAdapter(Context ctx) {
        this.context = ctx;
    }

    @NonNull
    @Override
    public FilterCategoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_filter_category, parent, false);
        return new MyViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull FilterCategoryAdapter.MyViewHolder holder, int position) {
        RadioButton rb = new RadioButton(FilterCategoryAdapter.this.context);
        rb.setText("Category");

        holder.txtCategoryName.addView(rb);
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        RadioGroup txtCategoryName;
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtCategoryName=itemView.findViewById(R.id.txtCategoryName);
            txtCategoryName.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    //since only one package is allowed to be selected
                    //this logic clears previous selection
                    //it checks state of last radiogroup and
                    // clears it if it meets conditions
                    if (lastCheckedRadioGroup != null
                            && lastCheckedRadioGroup.getCheckedRadioButtonId()
                            != radioGroup.getCheckedRadioButtonId()
                            && lastCheckedRadioGroup.getCheckedRadioButtonId() != -1) {
                        lastCheckedRadioGroup.clearCheck();
                    }
                    lastCheckedRadioGroup = radioGroup;

                }
            });
        }
    }
}
