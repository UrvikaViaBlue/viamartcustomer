package com.viamart.customer.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;


import com.viamart.customer.fragment.GroceryDetailsFragment;
import com.viamart.customer.model.StoreCategory;
import com.viamart.customer.model.StoreProductDetails;

import java.io.Serializable;
import java.util.List;

public class ShopeDetailsViewPager extends FragmentStatePagerAdapter {

    private int tabCount = 0;
    private Fragment fragment = null;
    private String category;
    private int Store_ID;
    Context context;
    private List<StoreCategory.Product> storeProductList;

    //Constructor to the class
    public ShopeDetailsViewPager(Context cntx,int Storeid, String category_id, List<StoreCategory.Product> datumList, FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
        category=category_id;
        Store_ID=Storeid;
        context=cntx;
        this.storeProductList=datumList;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        for (int i = 0; i < tabCount; i++) {
            if (i == position) {

                fragment = new GroceryDetailsFragment();
                Bundle bundle = new Bundle();
                bundle.putString("category_id",category);
                bundle.putInt("Store_ID",Store_ID);
                bundle.putSerializable("List", (Serializable) storeProductList);
                fragment.setArguments(bundle);
                break;
            }
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return tabCount;
    }

}
