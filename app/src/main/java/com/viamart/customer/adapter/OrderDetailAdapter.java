package com.viamart.customer.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.viamart.customer.R;
import com.viamart.customer.model.Order;

import java.util.List;

public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.MyVIewHolder> {
    Context context;
    List<Order.Data> orderDatas;

    public OrderDetailAdapter(List<Order.Data> orderData, Context ctx) {
        this.context = ctx;
        this.orderDatas = orderData;
    }

    @NonNull
    @Override
    public MyVIewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_order, parent, false);
        return new MyVIewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyVIewHolder holder, int position) {
        Order.Data ordeData =orderDatas.get(position);

        holder.txt_productName.setText(ordeData.getProducts().get(position).getProductName());
        if(ordeData.getProducts().get(position).getQuantity() != null) {
            holder.txt_productQuantity.setText(ordeData.getProducts().get(position).getQuantity());
        }else {
            holder.txt_productQuantity.setText("0");
        }
    }

    @Override
    public int getItemCount() {
        return orderDatas.size();
    }

    class MyVIewHolder extends RecyclerView.ViewHolder {
        TextView txt_productName, txt_productQuantity;

        MyVIewHolder(@NonNull View itemView) {
            super(itemView);
            txt_productName = itemView.findViewById(R.id.txt_productName);
            txt_productQuantity = itemView.findViewById(R.id.txt_productQuantity);
        }
    }


}