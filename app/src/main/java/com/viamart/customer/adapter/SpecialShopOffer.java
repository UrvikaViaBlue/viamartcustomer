package com.viamart.customer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.viamart.customer.R;
import com.viamart.customer.model.StoreDetails;

import java.util.List;

public class SpecialShopOffer extends RecyclerView.Adapter<SpecialShopOffer.MyViewHolder> {
    Context context;
    OnItemClickListener onItemClickListener;
    List<StoreDetails.CategoryDatum> categoryList;

    public SpecialShopOffer(List<StoreDetails.CategoryDatum> categoryData, Context ctx, OnItemClickListener onItemClick) {
        this.onItemClickListener = onItemClick;
        this.context = ctx;
        this.categoryList = categoryData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_special_shop_offer, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        StoreDetails.CategoryDatum categoryDatum = categoryList.get(position);

        if (categoryDatum.getCategoryImage() != null) {
            Glide.with(context)
                    .load(categoryDatum.getCategoryImage())
                    .into(holder.imgShopeOffer);

            holder.imgShopeOffer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(v, position);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView imgShopeOffer;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgShopeOffer = itemView.findViewById(R.id.imgShopeOffer);

        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

}
