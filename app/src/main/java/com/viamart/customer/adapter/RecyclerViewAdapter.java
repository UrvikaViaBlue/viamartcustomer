package com.viamart.customer.adapter;

import android.content.Context;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.viamart.customer.R;

import java.util.List;

import static com.viamart.customer.activity.AddAddressActivity.recycleItemClickListner;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private long mLastClickTime = 0;

    Context mcontext;
    //    int[] mimg;
    List<String> mplace;
    List<String> maddress;
    LayoutInflater mInflater;

    public RecyclerViewAdapter(Context context, List<String> place, List<String> address) {
        this.mcontext = context;
        this.mplace = place;
//        this.mimg=img;
        this.maddress = address;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = mInflater.inflate(R.layout.lay_address_dialog, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tv_address_place.setText(mplace.get(position));
        holder.tv_dialog_address.setText(maddress.get(position));
        switch (mplace.get(position)) {
            case "Work":
                holder.iv_address_img.setImageResource(R.drawable.ic_work);
                break;
            case "Other":
                holder.iv_address_img.setImageResource(R.drawable.ic_other);
                break;
            case "Home":
                holder.iv_address_img.setImageResource(R.drawable.ic_home);
                break;
        }

    }

    @Override
    public int getItemCount() {
        return mplace.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_address_place, tv_dialog_address;
        ImageView iv_address_img;

        ViewHolder(View itemView) {
            super(itemView);
            iv_address_img = itemView.findViewById(R.id.iv_address_img);
            tv_address_place = itemView.findViewById(R.id.tv_address_place);
            tv_dialog_address = itemView.findViewById(R.id.tv_dialog_address);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            // mis-clicking prevention, using threshold of 1000 ms
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();


            // if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            recycleItemClickListner.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position


    // allows clicks events to be caught
//    void setClickListener(ItemClickListener itemClickListener) {
//        //this.mClickListener = itemClickListener;
//    }

    // parent activity will implement this method to respond to click events

}
