package com.viamart.customer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.viamart.customer.R;
import com.viamart.customer.model.AddtoCartData;

import java.util.List;

public class CartListAdapter extends RecyclerView.Adapter<CartListAdapter.MyViewHolder> {
    private Context context;
    private List<AddtoCartData> addtoCartDataList;

    public CartListAdapter(Context ctx, List<AddtoCartData> addtoCartData) {
        this.context = ctx;
        this.addtoCartDataList = addtoCartData;
    }

    @NonNull
    @Override
    public CartListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grocery_details, parent, false);
        return new CartListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CartListAdapter.MyViewHolder holder, int position) {
        AddtoCartData addtoCartData = addtoCartDataList.get(position);
        /*Glide.with(context)
                .load(addtoCartData.getStoreProductData().)
                .into(holder.imgShop);*/
        for (int i = 0; i < addtoCartDataList.size(); i++) {
            for (int j = 0; j < addtoCartDataList.get(i).getStoreProductData().getVariantName().size(); j++) {
                holder.txtProductPrice.setText(String.format("%s RS",addtoCartDataList.get(i).getStoreProductData().getVariantName().get(j).getProductPrice()));
                holder.txtProductData.setText(addtoCartDataList.get(i).getStoreProductData().getVariantName().get(j).getVariantName());
            }
        }
        holder.txtProductName.setText(addtoCartData.getStoreProductData().getProductName());
       // holder.txtProductPrice.setText(String.valueOf(addtoCartData.getStoreProductData().getVarients().get(position).getVariantPrice()));
       // holder.txtProductData.setText(addtoCartData.getStoreProductData().getVarients().get(position).getVariantQty());
        holder.addItem.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return addtoCartDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView imgShop;
        TextView txtProductName, txtProductPrice, txtProductData;
        LinearLayout addItem;
        AppCompatSpinner spinner_product_QuantityDetails;
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgShop = itemView.findViewById(R.id.imgShop);
            txtProductName = itemView.findViewById(R.id.txtProductName);
            txtProductPrice = itemView.findViewById(R.id.txtProductPrice);
           // txtProductData = itemView.findViewById(R.id.txtProductData);
            txtProductData.setVisibility(View.VISIBLE);

            addItem=itemView.findViewById(R.id.addItem);
            spinner_product_QuantityDetails.setVisibility(View.GONE);

        }
    }
}
