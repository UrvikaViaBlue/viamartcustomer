package com.viamart.customer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.viamart.customer.R;

public class OfferShopeListAdapter extends RecyclerView.Adapter<OfferShopeListAdapter.MyVIewHolder> {
    Context context;
    OnItemClickListener onItemClickListener;

    public OfferShopeListAdapter(Context ctx, OnItemClickListener onItemClick) {
        this.context = ctx;
        this.onItemClickListener=onItemClick;
    }

    @NonNull
    @Override
    public OfferShopeListAdapter.MyVIewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shop_name, parent, false);
        return new MyVIewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OfferShopeListAdapter.MyVIewHolder holder, int position) {

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(v,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return 8;
    }

    class MyVIewHolder extends RecyclerView.ViewHolder {
        RelativeLayout layout;

        MyVIewHolder(@NonNull View itemView) {
            super(itemView);
            layout=itemView.findViewById(R.id.layout);
        }
    }

    public interface OnItemClickListener {
         void onItemClick(View view, int position);
    }
}
