package com.viamart.customer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.viamart.customer.R;

public class CountrySpinnerAdapter extends BaseAdapter {
    private Context context;
    private String[] countryNames;
    private LayoutInflater inflter;

    public CountrySpinnerAdapter(Context cont, String[] countryNames) {
        this.context = cont;
        this.countryNames = countryNames;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.custom_spinner_items_country, null);
        TextView names = (TextView) convertView.findViewById(R.id.textView);
        names.setText(countryNames[position]);
        return convertView;
    }
}
