package com.viamart.customer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.viamart.customer.R;
import com.viamart.customer.model.Order;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {
    Context context;
    OnItemClickListener onItemClickListener;
    List<Order.Product> productDataLists;

    public ProductAdapter(List<Order.Product> productData, Context ctx) {
        this.context = ctx;
        productDataLists = productData;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_order, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Order.Product storeDataList = productDataLists.get(position);

       holder.txt_productName.setText(storeDataList.getProductName());
       if(storeDataList.getQuantity() != null) {
           holder.txt_productQuantity.setText(storeDataList.getQuantity());
       }else {
           holder.txt_productQuantity.setText("0");
       }
    }

    @Override
    public int getItemCount() {
        return productDataLists.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_productName, txt_productQuantity;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_productName = itemView.findViewById(R.id.txt_productName);
            txt_productQuantity = itemView.findViewById(R.id.txt_productQuantity);
            // txtCity = itemView.findViewById(R.id.txtCity);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }


}
