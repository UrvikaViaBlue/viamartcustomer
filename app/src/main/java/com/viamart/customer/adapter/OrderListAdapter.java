package com.viamart.customer.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.viamart.customer.R;
import com.viamart.customer.model.Address;
import com.viamart.customer.model.Order;

import java.util.List;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.MyVIewHolder> {
    Context context;
    OnItemClickListener onItemClickListener;
    List<Order.Data> orderDatas;

    public OrderListAdapter(List<Order.Data> orderData, Context ctx, OnItemClickListener onItemClick) {
        this.context = ctx;
        this.onItemClickListener = onItemClick;
        this.orderDatas = orderData;
    }

    @NonNull
    @Override
    public MyVIewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_list, parent, false);
        return new MyVIewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyVIewHolder holder, int position) {
        Order.Data orderData = orderDatas.get(position);
        LinearLayoutManager mLayoutManager;

        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        holder.recyclerView.setLayoutManager(mLayoutManager);
        holder.recyclerView.setItemAnimator(new DefaultItemAnimator());
        holder.adapter = new ProductAdapter(orderData.getProducts(), context);
        holder.recyclerView.setAdapter(holder.adapter);

        holder.txtShopName.setText(orderData.getStoreName());
        holder.txtShopAddress.setText(orderData.getStoreAddress1()+" "+orderData.getStoreAddress2() + " "+orderData.getStoreCityName());
        holder.txt_DateTime.setText(orderData.getBookingDate()+ " "+orderData.getOrderTime());
        holder.txtOrderStatus.setText(orderData.getStatus());
        // holder.imgShop.setImageDrawable(orderData.get);
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(v,position);
            }
        });
    }

    @Override
    public int getItemCount() {

        return orderDatas.size();
    }

    class MyVIewHolder extends RecyclerView.ViewHolder {
        RelativeLayout layout;
        ImageView imgShop, imgOrderStatus;
        ProductAdapter adapter;
        RecyclerView recyclerView;
        TextView txtShopName, txtOrderStatus, txtShopAddress,  txt_DateTime;

        MyVIewHolder(@NonNull View itemView) {
            super(itemView);
            layout = itemView.findViewById(R.id.layout);
            recyclerView = itemView.findViewById(R.id.rv_item);
            txtShopName = itemView.findViewById(R.id.txtShopName);
            txtOrderStatus = itemView.findViewById(R.id.txtOrderStatus);
            txtShopAddress = itemView.findViewById(R.id.txtShopAddress);
            txt_DateTime = itemView.findViewById(R.id.txt_DateTime);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}