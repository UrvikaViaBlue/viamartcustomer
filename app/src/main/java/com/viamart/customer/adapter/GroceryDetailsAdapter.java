package com.viamart.customer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.viamart.customer.R;
import com.viamart.customer.model.AddtoCartData;
import com.viamart.customer.model.StoreCategory;

import java.util.List;

public class GroceryDetailsAdapter extends RecyclerView.Adapter<GroceryDetailsAdapter.MyViewHodler> implements AdapterView.OnItemSelectedListener {
    private Context context;
    ProdyctQuantityDetails prodyctQuantityDetails;
    OnItemClickListener onItemClickListener;
    List<StoreCategory.Product> storeProductData;
    List<AddtoCartData> addToCartList;

    public GroceryDetailsAdapter(List<StoreCategory.Product> list, List<AddtoCartData> addtoCartDataList,Context ctx, OnItemClickListener onItemClick) {
        this.context = ctx;
        this.onItemClickListener = onItemClick;
        this.storeProductData = list;
        this.addToCartList=addtoCartDataList;
    }

    @NonNull
    @Override
    public MyViewHodler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grocery_details, parent, false);
        return new MyViewHodler(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHodler holder, int position) {
        StoreCategory.Product storeProdct = storeProductData.get(position);

        //holder.txtCountity.setText(storeProdct.getProductQty());
        holder.txtProductName.setText(storeProdct.getProductName());
        final int[] minteger = {1};
        holder.increseData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minteger[0] = minteger[0] + 1;
                holder.txtCountity.setText(String.valueOf(minteger[0]));
                String quantity =holder.txtCountity.getText().toString();
                if(Integer.parseInt(quantity) > 0 ){
                    onItemClickListener.onItemClickAddItem(v, position,Integer.parseInt(quantity));
                }else {
                    onItemClickListener.onItemClickAddItem(v, position,0);
                }
            }
        });
        holder.addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               holder.addItem.setVisibility(View.GONE);
               holder.rl_itemAddRemove.setVisibility(View.VISIBLE);
               holder.txt_customizable.setVisibility(View.GONE);

                String quantity =holder.txtCountity.getText().toString();
                if(Integer.parseInt(quantity) > 0 ){
                    onItemClickListener.onItemClickAddItem(v, position,Integer.parseInt(quantity));
                }else {
                    onItemClickListener.onItemClickAddItem(v, position,0);
                }
            }
        });
        holder.decresingData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minteger[0] = minteger[0] - 1;
                if (minteger[0] <= 0) {
                    minteger[0] = 0;
                    holder.addItem.setVisibility(View.VISIBLE);
                    holder.rl_itemAddRemove.setVisibility(View.GONE);
                    holder.txt_customizable.setVisibility(View.VISIBLE);
                    holder.txtCountity.setText(String.valueOf(0));
                } else {
                    String quantity =holder.txtCountity.getText().toString();
                    if(Integer.parseInt(quantity) > 0 ){
                        onItemClickListener.onItemClickAddItem(v, position,Integer.parseInt(quantity));
                    }else {
                        onItemClickListener.onItemClickAddItem(v, position,0);
                    }
                    holder.txtCountity.setText(String.valueOf(minteger[0]));
                }
            }
        });
        /*
         * When data list more than one Quantity that time display this and hide textview
         * */
        ProdyctQuantityDetails customAdapter = new ProdyctQuantityDetails(context, storeProdct.getVariantName(), new ProdyctQuantityDetails.OnItemClickListener() {
            @Override
            public void onItemClickAddItem(View view, int position) {
                /*for (int i = 0; i <storeProdct.getVarients().size() ; i++) {
                    if(i== position){

                    }
                }*/
            }
        });
       /* holder.spinner_product_QuantityDetails.setAdapter(customAdapter);
        holder.spinner_product_QuantityDetails.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object item = parent.getItemAtPosition(position);
                holder.txtProductPrice.setText(String.format("%s RS", String.valueOf(storeProdct.getVariantName().get(position).getProductPrice())));
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });*/

    }


    @Override
    public int getItemCount() {
        return storeProductData.size();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    class MyViewHodler extends RecyclerView.ViewHolder {
        AppCompatTextView increseData, decresingData;
        AppCompatTextView txtCountity;
        /*LinearLayout addItem, add_item_data;*/
        TextView addItem, txt_customizable,txtProductName, txtProductPrice;
        RelativeLayout rl_itemAddRemove;
       // Spinner spinner_product_QuantityDetails;

        MyViewHodler(@NonNull View itemView) {
            super(itemView);
           // spinner_product_QuantityDetails = itemView.findViewById(R.id.spinner_product_QuantityDetails);
            txtProductName = itemView.findViewById(R.id.txtProductName);
            txt_customizable = itemView.findViewById(R.id.txt_customizable);
            txtProductPrice = itemView.findViewById(R.id.txtProductPrice);
          //  txtProductData = itemView.findViewById(R.id.txtProductData);
            increseData = itemView.findViewById(R.id.txt_AddQuantity);
            rl_itemAddRemove = itemView.findViewById(R.id.rl_itemAddRemove);
            decresingData = itemView.findViewById(R.id.Txt_MinusQuantity);
            txtCountity = itemView.findViewById(R.id.txt_quantity);
            addItem = itemView.findViewById(R.id.addItem);
           // addItem = itemView.findViewById(R.id.addItem);
            //add_item_data = itemView.findViewById(R.id.add_item_data);
        }
    }

    public interface OnItemClickListener {
        void onItemClickAddItem(View view, int position,int quantity);
    }
}
