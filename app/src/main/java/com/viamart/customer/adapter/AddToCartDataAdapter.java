package com.viamart.customer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.viamart.customer.R;
import com.viamart.customer.model.AddtoCartData;

import java.util.List;

public class AddToCartDataAdapter extends RecyclerView.Adapter<AddToCartDataAdapter.MyViewHolder> {
    private List<AddtoCartData> addtoCartDataList;
    Context context;

    public AddToCartDataAdapter(Context ctx, List<AddtoCartData> addtoCartData) {
        this.context = ctx;
        this.addtoCartDataList = addtoCartData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grocery_details, parent, false);
        return new AddToCartDataAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        AddtoCartData storeProdct = addtoCartDataList.get(position);
        for (int i = 0; i < addtoCartDataList.size(); i++) {
            for (int j = 0; j < addtoCartDataList.get(i).getStoreProductData().getVariantName().size(); j++) {
                //   holder.txtProductPrice.setText(String.format("%s RS", addtoCartDataList.get(i).getStoreProductData().getVariantName().get(j).getProductPrice()));
                // holder.txtProductData.setText(addtoCartDataList.get(i).getStoreProductData().getVariantName().get(j).getVariantName());
            }
        }
        holder.txtProductPrice.setText(String.valueOf(storeProdct.getVariantPrice()));
               // holder.txtProductData.setText();
        holder.txtProductName.setText(storeProdct.getStoreProductData().getProductName());
        /*final int[] minteger = {1};
        holder.decresingData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minteger[0] = minteger[0] - 1;
                if (minteger[0] < 0) {
                    minteger[0] = 0;
                    holder.addItem.setVisibility(View.VISIBLE);
                    holder.txtCountity.setText(String.valueOf(0));
                } else {
                    String quantity = holder.txtCountity.getText().toString();
                    if (Integer.parseInt(quantity) > 0) {
                    } else {
                    }
                    holder.txtCountity.setText(String.valueOf(minteger[0]));
                }
            }
        });

        holder.increseData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minteger[0] = minteger[0] + 1;
                holder.txtCountity.setText(String.valueOf(minteger[0]));
                String quantity = holder.txtCountity.getText().toString();
                if (Integer.parseInt(quantity) < 0) {
                    holder.add_item_data.setVisibility(View.GONE);
                } else {
                }
            }
        });
        if (storeProdct.getQuantity() > 0) {
            holder.txtCountity.setText(String.valueOf(storeProdct.getQuantity()));
        }*/
    }

    @Override
    public int getItemCount() {
        return addtoCartDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout rl_itemAddRemove;
        TextView txtProductName, txtProductPrice;
        LinearLayout add_item_data;
        AppCompatTextView addItem, txt_productDescription, txt_customizable;
        AppCompatImageView imgShop;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            addItem = itemView.findViewById(R.id.addItem);
            rl_itemAddRemove = itemView.findViewById(R.id.rl_itemAddRemove);
            rl_itemAddRemove.setVisibility(View.GONE);
            addItem.setVisibility(View.GONE);
            txtProductName = itemView.findViewById(R.id.txtProductName);
            txtProductPrice = itemView.findViewById(R.id.txtProductPrice);
            imgShop = itemView.findViewById(R.id.imgShop);
            imgShop.setVisibility(View.GONE);
            txt_customizable = itemView.findViewById(R.id.txt_customizable);
            txt_customizable.setVisibility(View.GONE);
          /*  txtCountity = itemView.findViewById(R.id.txtCountity);
            increseData = itemView.findViewById(R.id.increseData);
            decresingData = itemView.findViewById(R.id.decresingData);*/
        }
    }
}
