package com.viamart.customer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.viamart.customer.R;
import com.viamart.customer.model.AddtoCartData;

import java.util.List;

public class AddedItemListForCartAdapter extends RecyclerView.Adapter<AddedItemListForCartAdapter.MyViewHolder> {

    Context context;
    List<AddtoCartData> addtoCartDataList;
    OnItemClickListener onItemClickListener;

    public AddedItemListForCartAdapter(Context ctx, List<AddtoCartData> addtoCartList,OnItemClickListener onItemClick) {
        this.context = ctx;
        this.addtoCartDataList = addtoCartList;
        this.onItemClickListener = onItemClick;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grocery_details, parent, false);
        return new AddedItemListForCartAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        AddtoCartData addtoCartData=addtoCartDataList.get(position);


        holder.txtProductName.setText(addtoCartData.getStoreProductData().getProductName());
        final int[] minteger = {1};
        holder.increseData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minteger[0] = minteger[0] + 1;
                holder.txtCountity.setText(String.valueOf(minteger[0]));
                String quantity =holder.txtCountity.getText().toString();
                if(Integer.parseInt(quantity) > 0 ){
                    onItemClickListener.onItemClickAddItem(v, position,Integer.parseInt(quantity));
                }else {
                    onItemClickListener.onItemClickAddItem(v, position,0);
                }
            }
        });
        holder.decresingData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minteger[0] = minteger[0] - 1;
                if (minteger[0] <= 0) {
                    minteger[0] = 1;
                    holder.addItem.setVisibility(View.GONE);
                    holder.rl_itemAddRemove.setVisibility(View.GONE);
                    holder.txt_customizable.setVisibility(View.GONE);
                } else {
                    String quantity =holder.txtCountity.getText().toString();
                    if(Integer.parseInt(quantity) > 0 ){
                        onItemClickListener.onItemClickAddItem(v, position,Integer.parseInt(quantity));
                    }else {
                        onItemClickListener.onItemClickAddItem(v, position,0);
                    }
                    holder.txtCountity.setText(String.valueOf(minteger[0]));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return addtoCartDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView increseData, decresingData;
        AppCompatTextView txtCountity;
        /*LinearLayout addItem, add_item_data;*/
        TextView addItem, txt_customizable,txtProductName, txtProductPrice;
        RelativeLayout rl_itemAddRemove;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtProductName = itemView.findViewById(R.id.txtProductName);
            txt_customizable = itemView.findViewById(R.id.txt_customizable);
            txtProductPrice = itemView.findViewById(R.id.txtProductPrice);
            //  txtProductData = itemView.findViewById(R.id.txtProductData);
            increseData = itemView.findViewById(R.id.txt_AddQuantity);
            rl_itemAddRemove = itemView.findViewById(R.id.rl_itemAddRemove);
            decresingData = itemView.findViewById(R.id.Txt_MinusQuantity);
            txtCountity = itemView.findViewById(R.id.txt_quantity);
            addItem = itemView.findViewById(R.id.addItem);
            addItem.setVisibility(View.GONE);

        }
    }
    public interface OnItemClickListener {
        void onItemClickAddItem(View view, int position,int quantity);
    }
}
